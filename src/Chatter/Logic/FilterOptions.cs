﻿namespace Chatter.Logic
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct FilterOptions
    {
        public bool Elizabethan;
        public bool ProperEnglish;
        public bool Shorthand;
    }
}

