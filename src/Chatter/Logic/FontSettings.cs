﻿namespace Chatter.Logic
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct FontSettings
    {
        public FontSetting Channels;
        public FontSetting ChatText;
        public FontSetting UserList;
    }
}

