﻿namespace Chatter.Logic
{
    using System;
    using System.IO;

    internal static class Directories
    {
        public static string GetPluginSettingsDirectory(string plugin) => 
            Path.Combine(PluginSettingsRootDirectory, plugin);

        public static string GetPluginSettingsFilename(string plugin) => 
            Path.Combine(GetPluginSettingsDirectory(plugin), "Settings.xml");

        public static string ProgramDirectory =>
            AppDomain.CurrentDomain.BaseDirectory;

        public static string PluginDirectory =>
            Path.Combine(ProgramDirectory, "Plugins");

        public static string ProgramSettingsDirectory =>
            Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Illusory Studios"), "Chatter");

        public static string ProgramSettingsFilename =>
            Path.Combine(ProgramSettingsDirectory, "Settings.xml");

        public static string PluginSettingsRootDirectory =>
            Path.Combine(ProgramSettingsDirectory, "Plugins");
    }
}

