﻿namespace Chatter.Logic
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct UserSettings
    {
        public string Nickname;
        public string RealName;
    }
}

