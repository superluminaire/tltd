﻿namespace Chatter.Logic
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct FontSetting
    {
        public string Name;
        public float Size;
    }
}

