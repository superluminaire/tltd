﻿namespace Chatter.Logic
{
    using System;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Xml.Serialization;

    internal static class SettingsIO
    {
        public static bool TryLoad(string path, Type type, out object settings)
        {
            Stream stream;
            settings = null;
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(path));
            }
            catch (Exception)
            {
                return false;
            }
            try
            {
                stream = File.Open(path, FileMode.Open, FileAccess.Read);
            }
            catch (Exception)
            {
                return false;
            }
            using (stream)
            {
                try
                {
                    settings = new XmlSerializer(type).Deserialize(stream);
                }
                catch (InvalidOperationException)
                {
                    return false;
                }
                return true;
            }
        }

        public static bool TrySave(string path, object settings)
        {
            Stream stream;
            try
            {
                stream = File.Create(path);
            }
            catch (Exception)
            {
                return false;
            }
            using (stream)
            {
                try
                {
                    new XmlSerializer(settings.GetType()).Serialize(stream, settings);
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            }
        }
    }
}

