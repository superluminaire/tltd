﻿namespace Chatter.Logic
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class Settings
    {
        public LanguageOptions Language = new LanguageOptions();
        public IdentdSettings Identd;
        public UserSettings UserInfo;
        public ServerSettings Servers;
        public FontSettings Fonts;

        static Settings()
        {
            object obj2;
            SettingsIO.TryLoad(Directories.ProgramSettingsFilename, typeof(Settings), out obj2);
            Instance = ((Settings) obj2) ?? new Settings();
        }

        private Settings()
        {
            IdentdSettings settings = new IdentdSettings {
                Enabled = true,
                OperatingSystem = "UNIX",
                UserID = "johndoe"
            };
            this.Identd = settings;
            UserSettings settings2 = new UserSettings {
                Nickname = "YourNameHere",
                RealName = "John Doe"
            };
            this.UserInfo = settings2;
            this.Servers = new ServerSettings();
            FontSettings settings3 = new FontSettings();
            FontSetting setting = new FontSetting {
                Name = "Verdana",
                Size = 9.75f
            };
            settings3.Channels = setting;
            setting = new FontSetting {
                Name = "Verdana",
                Size = 9.75f
            };
            settings3.ChatText = setting;
            setting = new FontSetting {
                Name = "Verdana",
                Size = 9.75f
            };
            settings3.UserList = setting;
            this.Fonts = settings3;
        }

        public void Save()
        {
            SettingsIO.TrySave(Directories.ProgramSettingsFilename, this);
        }

        public static Settings Instance { get; private set; }
    }
}

