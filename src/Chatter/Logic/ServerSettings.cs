﻿namespace Chatter.Logic
{
    using System;

    public class ServerSettings
    {
        public string Default = "Freenode";
        public ServerEntry[] Listing;

        public ServerSettings()
        {
            ServerEntry entry1 = new ServerEntry();
            entry1.Host = "irc.freenode.net";
            entry1.Description = "Freenode";
            entry1.PortRange = "7070";
            entry1.SSL = true;
            ServerEntry[] entryArray1 = new ServerEntry[4];
            entryArray1[0] = entry1;
            ServerEntry entry2 = new ServerEntry();
            entry2.Host = "irc.oftc.net";
            entry2.Description = "OFTC";
            entryArray1[1] = entry2;
            ServerEntry entry3 = new ServerEntry();
            entry3.Host = "irc.rizon.net";
            entry3.Description = "Rizon";
            entryArray1[2] = entry3;
            ServerEntry entry4 = new ServerEntry();
            entry4.Host = "irc.quakenet.org";
            entry4.Description = "QuakeNet";
            entry4.PortRange = "6667-6669";
            entryArray1[3] = entry4;
            this.Listing = entryArray1;
        }
    }
}

