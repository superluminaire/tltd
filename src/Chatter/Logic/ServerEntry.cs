﻿namespace Chatter.Logic
{
    using System;

    public class ServerEntry : IComparable
    {
        public string Host;
        public string Description;
        public string PortRange = "6667";
        public bool SSL;

        public int CompareTo(object o) => 
            this.ToString().CompareTo(o.ToString());

        public override string ToString() => 
            (this.Description != "") ? ((this.Host != "") ? (this.Description + " - " + this.Host) : this.Description) : this.Host;
    }
}

