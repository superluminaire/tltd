﻿namespace Chatter.Logic
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct IdentdSettings
    {
        public bool Enabled;
        public string OperatingSystem;
        public string UserID;
    }
}

