﻿namespace TLTD
{
    using Chatter.Logic;
    using FluentFTP;
    using NetIrc2;
    using NetIrc2.Events;
    using Renci.SshNet;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Security;
    using System.Net.Sockets;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Security.Authentication;
    using System.Security.Cryptography.X509Certificates;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;
    using System.Xml;
    using TradeWright.UI.Forms;
    using WK.Libraries.BetterFolderBrowserNS;

    public class TLTD : Form
    {
        private string trueNickname;
        private int AltNick;
        private int torrentsseen;
        private int torrentsactioned;
        private bool offlinewanted;
        private int retries;
        private bool settingschanged;
        private string commonpath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        private string debugfilename;
        private Rectangle dragBoxFromMouseDown;
        private int rowIndexFromMouseDown;
        private int rowIndexOfItemUnderMouseToDrop;
        private IContainer components;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private Panel panel1;
        private GroupBox groupBox1;
        private ToolStripStatusLabel StatusLabel;
        private ToolStripStatusLabel toolStripStatusLabel4;
        private ToolStripStatusLabel TorrentsSeen;
        private ToolStripStatusLabel toolStripStatusLabel6;
        private ToolStripStatusLabel TorrentsActioned;
        private Button GoOfflineButton;
        private Button GoOnlineButton;
        private TextBox RSSKey;
        private Label label3;
        private Label label2;
        private Label label1;
        private TextBox BotNickname;
        private TextBox SiteUsername;
        private IrcClient ircClient;
        private ImageList imageList1;
        private TabControlExtra MainTabControl;
        private TabPage AnnouncedTorrentsTab;
        private ListView TorrentList;
        private ColumnHeader columnHeader1;
        private ColumnHeader columnHeader2;
        private ColumnHeader columnHeader3;
        private ColumnHeader columnHeader4;
        private ColumnHeader columnHeader5;
        private ColumnHeader columnHeader6;
        private TabPage FiltersTab;
        private DataGridView Filters;
        private RichTextBox FilterHelp;
        private GroupBox RegexToolbox;
        private Label MatchOrNoMatch;
        private Label label13;
        private Label label12;
        private TextBox TestString;
        private TextBox RegularExpression;
        private ToolStrip FiltersToolbar;
        private ToolStripButton AddFilterButton;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripButton ToggleFilterHelpButton;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripButton SaveSettingsButton2;
        private TabPage SettingsTab;
        private Panel panel2;
        private GroupBox groupBox8;
        private Label label11;
        private TextBox AnnounceRegex;
        private Label label9;
        private Label label8;
        private TextBox Announcers;
        private TextBox AnnounceChan;
        private CheckBox UseTLS;
        private Label label7;
        private NumericUpDown IRCPort;
        private Label label6;
        private TextBox IRCServer;
        private GroupBox groupBox7;
        private RichTextBox richTextBox1;
        private GroupBox groupBox6;
        private Label label15;
        private Label ThemeLabel;
        private ComboBox Theme;
        private CheckBox UseRegexFilters;
        private ToolStrip SettingsToolbar;
        private ToolStripButton SaveSettingsButton1;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripButton RefreshLanguagesAndThemesButton;
        private Label DebugLevelText;
        private NumericUpDown DebugLevel;
        private ComboBox Language;
        private Label LanguageLabel;
        private TabPage DebugTab;
        private RichTextBox DebugLog;
        private GroupBox groupBox5;
        private RichTextBox richTextBox2;
        private Label label4;
        private Panel panel3;
        private ToolStrip DefaultActionToolbar;
        private ToolStripButton FolderBrowseButton;
        private ToolStripTextBox Target;
        private ComboBox DefaultAction;
        private Label DefaultActionLabel;
        private CheckBox IgnoreInvalidTLSCertificates;
        private NotifyIcon notifyIcon1;
        private ComboBox DoubleClickAction;
        private Label label5;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem ActionDefault;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripMenuItem ActionRemove;
        private ToolStripMenuItem ActionRemoveUnmatched;
        private ToolStripMenuItem ActionRemoveAll;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripMenuItem ActionTest;
        private ToolStripMenuItem ActionOpenURL;
        private ToolStripMenuItem ActionSaveToTarget;
        private ComboBox BaseURL;
        private Label label14;
        private System.Windows.Forms.Timer IdleTimer;
        private ToolStripMenuItem ActionOpenInTorrentClient;
        private DataGridViewCheckBoxColumn FilterEnabled;
        private DataGridViewComboBoxColumn FilterFreeleech;
        private DataGridViewTextBoxColumn FilterTitle;
        private DataGridViewTextBoxColumn FilterCategory;
        private DataGridViewTextBoxColumn FilterExcludeTitle;
        private DataGridViewTextBoxColumn FilterExcludeCategory;
        private DataGridViewTextBoxColumn FilterMinSize;
        private DataGridViewTextBoxColumn FilterMaxSize;
        private DataGridViewComboBoxColumn Action;
        private DataGridViewTextBoxColumn IndividualTarget;
        private CheckBox MinimizeToSystemTray;
        private CheckBox SaveSettingsOnQuit;
        private CheckBox RememberApplicationSizeAndPosition;
        private CheckBox StartMinimized;
        private CheckBox WarnOnQuitIfSettingsNotSaved;
        private Label label16;
        private ComboBox FreeUnitOfMeasure;
        private NumericUpDown FreeRequirement;
        private CheckBox RequireAtLeast;
        private ToolStripStatusLabel toolStripStatusLabel2;
        private ToolStripStatusLabel WarningsOrErrors;
        private Label label10;
        private CheckBox SaveDebugLog;
        private NumericUpDown DeleteLogDays;
        private CheckBox DeleteLogsOlderThan;
        private CheckBox GoOnlineAtStartup;
        private ToolStripMenuItem ActionRemoveNegatedReleases;
        private ToolStripMenuItem ActionSelectAll;
        private ToolTip toolTip1;

        public TLTD()
        {
            this.InitializeComponent();
        }

        private void ActionDefault_Click(object sender, EventArgs e)
        {
            this.PerformActions(((ToolStripMenuItem) sender).Text);
        }

        private void ActionRemove_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in this.TorrentList.SelectedItems)
            {
                this.TorrentList.Items.Remove(item);
            }
        }

        private void ActionRemoveAll_Click(object sender, EventArgs e)
        {
            this.TorrentList.Items.Clear();
        }

        private void ActionRemoveNegatedReleases_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in this.TorrentList.Items)
            {
                if (item.ImageIndex == 2)
                {
                    this.TorrentList.Items.Remove(item);
                }
            }
        }

        private void ActionRemoveUnmatched_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in this.TorrentList.Items)
            {
                if (item.ImageIndex == 0)
                {
                    this.TorrentList.Items.Remove(item);
                }
            }
        }

        private void ActionSelectAll_Click(object sender, EventArgs e)
        {
            using (IEnumerator enumerator = this.TorrentList.Items.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    ((ListViewItem) enumerator.Current).Selected = true;
                }
            }
        }

        private void AddFilterButton_Click(object sender, EventArgs e)
        {
            object[] values = new object[10];
            values[0] = false;
            values[1] = "Don't Care";
            values[2] = "";
            values[3] = "";
            values[4] = "";
            values[5] = "";
            values[6] = "";
            values[7] = "";
            values[8] = "Use Default";
            values[9] = "";
            this.Filters.Rows.Add(values);
        }

        private void AddRelease(string release)
        {
            string action = "Do Nothing";
            string tempfile = "";
            string text = this.Target.Text;
            this.IdleTimer.Enabled = false;
            release = release.Replace("\x0002\x000300,04", "").Replace("\x0002\x000300,12", "").Replace("\x000301,15 ", "");
            this.LogDebug(release, 2);
            Match match = new Regex(this.AnnounceRegex.Text).Match(release);
            if (!match.Success)
            {
                this.LogDebug("No Regex Match.  Announcement received was not what was expected.", 3);
            }
            else
            {
                this.LogDebug("Regex Match Successful!  Groups: " + match.Groups.Count.ToString(), 3);
                if (this.DebugLevel.Value >= 4M)
                {
                    for (int i = 0; i < match.Groups.Count; i++)
                    {
                        this.LogDebug("Group " + i.ToString() + " : " + match.Groups[i].ToString(), 4);
                    }
                }
                ListViewItem item1 = this.TorrentList.Items.Add(DateTime.Now.ToString());
                item1.SubItems.Add(match.Groups[1].ToString());
                item1.SubItems.Add(match.Groups[2].ToString());
                item1.SubItems.Add(match.Groups[3].ToString());
                item1.SubItems.Add(match.Groups[4].ToString());
                item1.SubItems.Add(match.Groups[6].ToString());
                int num = this.IsMatch(match.Groups, out action, out tempfile, out text);
                item1.ImageIndex = num;
                if (num == 1)
                {
                    string title = match.Groups[2].ToString();
                    string torrentid = match.Groups[6].ToString();
                    string targeturl = "https://" + this.BaseURL.Text + "/torrent/" + torrentid;
                    if (action == "Use Default")
                    {
                        action = this.DefaultAction.Text;
                    }
                    this.PerformAction(action, title, torrentid, text, targeturl);
                    this.torrentsactioned++;
                    this.TorrentsActioned.Text = this.torrentsactioned.ToString();
                }
                this.TorrentList.EnsureVisible(this.TorrentList.Items.Count - 1);
            }
            this.torrentsseen++;
            this.TorrentsSeen.Text = this.torrentsseen.ToString();
            this.IdleTimer.Enabled = true;
        }

        private void ChangeTabTheme(TLTD.ColorScheme scheme, TabControl tabcontrol)
        {
            foreach (TabPage page1 in tabcontrol.TabPages)
            {
                page1.BackColor = scheme.TabPageBG;
                page1.ForeColor = scheme.TabPageFG;
            }
        }

        private void ChangeTheme(TLTD.ColorScheme scheme, Control.ControlCollection container)
        {
            foreach (Control control in container)
            {
                switch (control)
                {
                    case (Panel _):
                        this.ChangeTheme(scheme, control.Controls);
                        control.BackColor = scheme.PanelBG;
                        control.ForeColor = scheme.PanelFG;
                        continue;
                        break;
                }
                if (control is Button)
                {
                    control.BackColor = scheme.ButtonBG;
                    control.ForeColor = scheme.ButtonFG;
                    continue;
                }
                if (control is TextBox)
                {
                    control.BackColor = scheme.TextBoxBG;
                    control.ForeColor = scheme.TextBoxFG;
                    continue;
                }
                if (control is Form)
                {
                    control.BackColor = scheme.FormBG;
                    control.ForeColor = scheme.FormFG;
                    continue;
                }
                if (control is RichTextBox)
                {
                    control.BackColor = scheme.RichTextBG;
                    control.ForeColor = scheme.RichTextFG;
                }
            }
        }

        internal bool ConnectTo(string Hostname, int Port, bool SSL)
        {
            bool flag;
            this.Disconnect();
            this.ResetTrueName();
            IrcClientConnectionOptions options = new IrcClientConnectionOptions {
                Ssl = SSL,
                SslCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => (sslPolicyErrors != SslPolicyErrors.None) ? ((!this.IgnoreInvalidTLSCertificates.Checked || (sslPolicyErrors == SslPolicyErrors.None)) ? (MessageBox.Show(this, "There is a problem with the server's SSL certificate.\n\nContinue anyway?", "Certificate Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) : true) : true,
                SslHostname = Hostname
            };
            try
            {
                this.ircClient.Connect(Hostname, Port, options);
                return true;
            }
            catch (AuthenticationException exception)
            {
                this.LogDebug("Authentication Exception occurred: " + exception.Message, 0);
                this.Disconnect();
                flag = false;
            }
            catch (IOException exception2)
            {
                this.LogDebug("IO Exception occurred: " + exception2.Message, 0);
                this.Disconnect();
                flag = false;
            }
            catch (SocketException exception3)
            {
                this.LogDebug("Socket Exception occurred: " + exception3.Message, 0);
                this.Disconnect();
                flag = false;
            }
            catch (Exception exception4)
            {
                this.LogDebug("Exception Occurred: " + exception4.Message, 0);
                flag = false;
            }
            return flag;
        }

        private void contextMenuStrip1_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            using (IEnumerator enumerator = this.contextMenuStrip1.Items.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    ((ToolStripItem) enumerator.Current).Enabled = true;
                }
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            if (this.TorrentList.SelectedItems.Count == 0)
            {
                this.ActionDefault.Enabled = false;
                this.ActionRemove.Enabled = false;
                this.ActionTest.Enabled = false;
                this.ActionOpenURL.Enabled = false;
                this.ActionSaveToTarget.Enabled = false;
                this.ActionOpenInTorrentClient.Enabled = false;
            }
            if (this.TorrentList.Items.Count == 0)
            {
                this.ActionRemoveNegatedReleases.Enabled = false;
                this.ActionRemoveUnmatched.Enabled = false;
                this.ActionRemoveAll.Enabled = false;
                this.ActionSelectAll.Enabled = false;
            }
        }

        private void DeleteOldLogs()
        {
            foreach (FileInfo info in new DirectoryInfo(this.commonpath + @"\TLTD\Logs\").GetFiles("*.log"))
            {
                if ((DateTime.UtcNow - info.CreationTimeUtc) > TimeSpan.FromDays((double) this.DeleteLogDays.Value))
                {
                    try
                    {
                        System.IO.File.Delete(info.FullName);
                        this.LogDebug("Deleted old log file " + info.Name, 1);
                    }
                    catch (Exception exception)
                    {
                        this.LogDebug("Failed to delete log file " + info.Name + " - error: " + exception.Message, 1);
                    }
                }
            }
        }

        internal void Disconnect()
        {
            this.ircClient.LogOut(null);
            this.ircClient.Close();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void DoubleClickAction_TextChanged(object sender, EventArgs e)
        {
            this.ActionDefault.Text = this.DoubleClickAction.Text;
        }

        private string DownloadTorrentToTemp(string Name, string BaseURL, string TorrentID, string RSSKey)
        {
            string str3;
            if (!Regex.IsMatch(RSSKey, "[0-9a-f]{20}"))
            {
                this.LogDebug("Your RSS Key is empty, or has the wrong format.  Please put your RSS key in for this application to function.", 0);
                return "fail";
            }
            Name = Name.Replace(" ", ".") + ".torrent";
            string[] textArray1 = new string[9];
            textArray1[0] = "https://";
            textArray1[1] = BaseURL;
            textArray1[2] = "rss/download/";
            textArray1[3] = TorrentID;
            textArray1[4] = "/";
            textArray1[5] = RSSKey;
            textArray1[6] = "/";
            textArray1[7] = Name;
            textArray1[8] = ".torrent";
            string address = string.Concat(textArray1);
            string fileName = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            WebClient client = new WebClient();
            try
            {
                this.LogDebug("Attempting to download from " + address + " to " + fileName, 7);
                client.DownloadFile(address, fileName);
                this.LogDebug(client.ResponseHeaders.ToString(), 8);
                this.LogDebug("Download successful", 7);
                str3 = fileName;
            }
            catch (Exception exception1)
            {
                string message = exception1.Message;
                this.LogDebug("Download unsuccessful.  Error " + message, 7);
                if (Regex.IsMatch(message, @"\(500\)\ Internal\ Server\ Error"))
                {
                    MessageBox.Show(this, "The server retured a 500 Error.  This is usually because your RSS key is incorrect, or RSS is disabled.  Please re-enter your RSS key, and remember that your RSS key is NOT your IRC key.", "RSS Problem", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                return "fail";
            }
            return str3;
        }

        private bool EnoughDiskSpace(string targetpath, long sizeoftorrent)
        {
            if (this.RequireAtLeast.Checked)
            {
                ulong num2;
                ulong num3;
                ulong num4;
                string val = this.FreeRequirement.Value.ToString();
                val = (this.FreeUnitOfMeasure.Text != "Gigabytes (GB)") ? (val + "T") : (val + "G");
                long num = this.Numerize(val);
                if (string.IsNullOrEmpty(targetpath))
                {
                    targetpath = Path.GetTempPath();
                }
                string[] textArray1 = new string[] { "Checking if there is ", this.FreeRequirement.Value.ToString(), " ", this.FreeUnitOfMeasure.Text, " free on path ", targetpath };
                this.LogDebug(string.Concat(textArray1), 0);
                targetpath = targetpath.Replace(@"\", @"\\");
                if (!targetpath.EndsWith(@"\\"))
                {
                    targetpath = targetpath + @"\\";
                }
                if (!GetDiskFreeSpaceEx(targetpath, out num2, out num3, out num4))
                {
                    this.WarningsOrErrors.Text = "Errors reading free space on target - see log.";
                    this.WarningsOrErrors.Font = new Font(this.WarningsOrErrors.Font, FontStyle.Bold);
                    this.LogDebug("Failed to retrieve the size of " + targetpath, 0);
                    return false;
                }
                this.LogDebug($"Free Bytes Available:      {num2}", 1);
                this.LogDebug($"Total Number Of Bytes:     {num3}", 1);
                this.LogDebug($"Minimum Free Size:         {num}", 1);
                this.LogDebug($"Torrent Size:              {sizeoftorrent}", 1);
                if ((long.Parse(num2.ToString()) <= num) || (long.Parse(num2.ToString()) <= sizeoftorrent))
                {
                    this.LogDebug("There is insufficient Disk Space Available to action this release :(", 1);
                    this.WarningsOrErrors.Text = "Insufficient Disk Space Free!";
                    this.WarningsOrErrors.Font = new Font(this.WarningsOrErrors.Font, FontStyle.Bold);
                    return false;
                }
                this.LogDebug("There is sufficient Disk Space Available to action this release :)", 1);
                this.WarningsOrErrors.Text = "None";
                this.WarningsOrErrors.Font = new Font(this.WarningsOrErrors.Font, FontStyle.Regular);
            }
            return true;
        }

        private void Filters_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.Filters.IsCurrentCellDirty)
            {
                this.Filters.CommitEdit(DataGridViewDataErrorContexts.Commit);
                this.settingschanged = true;
            }
        }

        private void Filters_DragDrop(object sender, DragEventArgs e)
        {
            Point point = this.Filters.PointToClient(new Point(e.X, e.Y));
            this.rowIndexOfItemUnderMouseToDrop = this.Filters.HitTest(point.X, point.Y).RowIndex;
            if ((e.Effect == DragDropEffects.Move) && (this.rowIndexOfItemUnderMouseToDrop != -1))
            {
                DataGridViewRow data = e.Data.GetData(typeof(DataGridViewRow)) as DataGridViewRow;
                this.Filters.Rows.RemoveAt(this.rowIndexFromMouseDown);
                this.Filters.Rows.Insert(this.rowIndexOfItemUnderMouseToDrop, data);
            }
        }

        private void Filters_DragEnter(object sender, DragEventArgs e)
        {
        }

        private void Filters_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void Filters_MouseClick(object sender, MouseEventArgs e)
        {
        }

        private void Filters_MouseDown(object sender, MouseEventArgs e)
        {
            this.rowIndexFromMouseDown = this.Filters.HitTest(e.X, e.Y).RowIndex;
            if (this.rowIndexFromMouseDown == -1)
            {
                this.dragBoxFromMouseDown = Rectangle.Empty;
            }
            else
            {
                Size dragSize = SystemInformation.DragSize;
                this.dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2), e.Y - (dragSize.Height / 2)), dragSize);
            }
        }

        private void Filters_MouseMove(object sender, MouseEventArgs e)
        {
            if (((e.Button & MouseButtons.Left) == MouseButtons.Left) && ((this.dragBoxFromMouseDown != Rectangle.Empty) && !this.dragBoxFromMouseDown.Contains(e.X, e.Y)))
            {
                this.Filters.DoDragDrop(this.Filters.Rows[this.rowIndexFromMouseDown], DragDropEffects.Move);
            }
        }

        private void FolderBrowseButton_Click(object sender, EventArgs e)
        {
            BetterFolderBrowser browser = new BetterFolderBrowser {
                Title = "Browse for Target Folder."
            };
            if (browser.ShowDialog() == DialogResult.OK)
            {
                this.Target.Text = browser.SelectedFolder;
            }
        }

        private bool FTPUpload(string proto, string username, string password, string host, string path, string tempfile, string filename)
        {
            string s = "21";
            int result = 0x15;
            string remotePath = path;
            if (!remotePath.EndsWith("/"))
            {
                remotePath = remotePath + "/";
            }
            remotePath = remotePath + filename + ".torrent";
            char[] separator = new char[] { ':' };
            string[] strArray = host.Split(separator);
            if (strArray.Length == 2)
            {
                host = strArray[0];
                s = strArray[1];
            }
            if ((proto != "scp") && (proto != "sftp"))
            {
                FtpClient client2 = new FtpClient(host, username, password);
                if (proto == "ftp")
                {
                    client2.EncryptionMode = FtpEncryptionMode.None;
                }
                else if (s == "990")
                {
                    client2.EncryptionMode = FtpEncryptionMode.Implicit;
                    client2.SslProtocols = SslProtocols.Tls12;
                }
                else
                {
                    client2.EncryptionMode = FtpEncryptionMode.Explicit;
                    client2.SslProtocols = SslProtocols.Tls12;
                }
                int.TryParse(s, out result);
                client2.Port = result;
                client2.OnLogEvent = new Action<FtpTraceLevel, string>(this.OnFTPLogEvent);
                try
                {
                    client2.Connect();
                    if (client2.IsConnected)
                    {
                        if (!client2.DirectoryExists(path))
                        {
                            client2.CreateDirectory(path);
                        }
                        client2.SetWorkingDirectory(path);
                        client2.UploadFile(tempfile, remotePath, FtpRemoteExists.Overwrite, false, FtpVerify.None, null);
                        client2.Disconnect();
                        return true;
                    }
                }
                catch (Exception exception2)
                {
                    this.LogDebug("Failed to upload file to host " + host + " - error: " + exception2.Message, 0);
                    this.LogDebug($"Client Last Error: {client2.LastReply}", 0);
                    this.LogDebug($"InnerException: {exception2.InnerException}", 0);
                }
            }
            else
            {
                try
                {
                    using (SftpClient client = new SftpClient(host, username, password))
                    {
                        client.Connect();
                        using (FileStream stream = System.IO.File.OpenRead(tempfile))
                        {
                            client.UploadFile(stream, remotePath, null);
                        }
                    }
                    return true;
                }
                catch (Exception exception)
                {
                    this.LogDebug($"Error: {exception}", 0);
                }
            }
            return false;
        }

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("kernel32.dll", CharSet=CharSet.Auto, SetLastError=true)]
        private static extern bool GetDiskFreeSpaceEx(string lpDirectoryName, out ulong lpFreeBytesAvailable, out ulong lpTotalNumberOfBytes, out ulong lpTotalNumberOfFreeBytes);
        private void GoOfflineButton_Click(object sender, EventArgs e)
        {
            this.GoOfflineButton.Enabled = false;
            this.offlinewanted = true;
            this.Disconnect();
        }

        private void GoOnlineButton_Click(object sender, EventArgs e)
        {
            this.GoOnlineButton.Enabled = false;
            Settings.Instance.Identd.UserID = this.SiteUsername.Text;
            Settings.Instance.UserInfo.RealName = this.Text;
            Settings.Instance.UserInfo.Nickname = this.BotNickname.Text;
            if (this.ConnectTo(this.IRCServer.Text, int.Parse(this.IRCPort.Value.ToString()), this.UseTLS.Checked))
            {
                this.Connection.LogIn(Settings.Instance.Identd.UserID, Settings.Instance.UserInfo.RealName, Settings.Instance.UserInfo.Nickname, null, null, null);
            }
            else
            {
                this.GoOnlineButton.Enabled = true;
            }
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(TLTD.TLTD));
            DataGridViewCellStyle style = new DataGridViewCellStyle();
            DataGridViewCellStyle style2 = new DataGridViewCellStyle();
            this.statusStrip1 = new StatusStrip();
            this.toolStripStatusLabel1 = new ToolStripStatusLabel();
            this.StatusLabel = new ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new ToolStripStatusLabel();
            this.TorrentsSeen = new ToolStripStatusLabel();
            this.toolStripStatusLabel6 = new ToolStripStatusLabel();
            this.TorrentsActioned = new ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new ToolStripStatusLabel();
            this.WarningsOrErrors = new ToolStripStatusLabel();
            this.panel1 = new Panel();
            this.panel3 = new Panel();
            this.DefaultActionToolbar = new ToolStrip();
            this.FolderBrowseButton = new ToolStripButton();
            this.Target = new ToolStripTextBox();
            this.DefaultAction = new ComboBox();
            this.DefaultActionLabel = new Label();
            this.groupBox1 = new GroupBox();
            this.GoOfflineButton = new Button();
            this.imageList1 = new ImageList(this.components);
            this.GoOnlineButton = new Button();
            this.RSSKey = new TextBox();
            this.label3 = new Label();
            this.label2 = new Label();
            this.label1 = new Label();
            this.BotNickname = new TextBox();
            this.SiteUsername = new TextBox();
            this.ircClient = new IrcClient();
            this.MainTabControl = new TabControlExtra();
            this.AnnouncedTorrentsTab = new TabPage();
            this.TorrentList = new ListView();
            this.columnHeader1 = new ColumnHeader();
            this.columnHeader2 = new ColumnHeader();
            this.columnHeader3 = new ColumnHeader();
            this.columnHeader4 = new ColumnHeader();
            this.columnHeader5 = new ColumnHeader();
            this.columnHeader6 = new ColumnHeader();
            this.contextMenuStrip1 = new ContextMenuStrip(this.components);
            this.ActionDefault = new ToolStripMenuItem();
            this.ActionSelectAll = new ToolStripMenuItem();
            this.toolStripSeparator4 = new ToolStripSeparator();
            this.ActionRemove = new ToolStripMenuItem();
            this.ActionRemoveUnmatched = new ToolStripMenuItem();
            this.ActionRemoveNegatedReleases = new ToolStripMenuItem();
            this.ActionRemoveAll = new ToolStripMenuItem();
            this.toolStripSeparator5 = new ToolStripSeparator();
            this.ActionTest = new ToolStripMenuItem();
            this.ActionOpenURL = new ToolStripMenuItem();
            this.ActionSaveToTarget = new ToolStripMenuItem();
            this.ActionOpenInTorrentClient = new ToolStripMenuItem();
            this.FiltersTab = new TabPage();
            this.Filters = new DataGridView();
            this.FilterEnabled = new DataGridViewCheckBoxColumn();
            this.FilterFreeleech = new DataGridViewComboBoxColumn();
            this.FilterTitle = new DataGridViewTextBoxColumn();
            this.FilterCategory = new DataGridViewTextBoxColumn();
            this.FilterExcludeTitle = new DataGridViewTextBoxColumn();
            this.FilterExcludeCategory = new DataGridViewTextBoxColumn();
            this.FilterMinSize = new DataGridViewTextBoxColumn();
            this.FilterMaxSize = new DataGridViewTextBoxColumn();
            this.Action = new DataGridViewComboBoxColumn();
            this.IndividualTarget = new DataGridViewTextBoxColumn();
            this.FilterHelp = new RichTextBox();
            this.RegexToolbox = new GroupBox();
            this.MatchOrNoMatch = new Label();
            this.label13 = new Label();
            this.label12 = new Label();
            this.TestString = new TextBox();
            this.RegularExpression = new TextBox();
            this.FiltersToolbar = new ToolStrip();
            this.AddFilterButton = new ToolStripButton();
            this.toolStripSeparator1 = new ToolStripSeparator();
            this.ToggleFilterHelpButton = new ToolStripButton();
            this.toolStripSeparator2 = new ToolStripSeparator();
            this.SaveSettingsButton2 = new ToolStripButton();
            this.SettingsTab = new TabPage();
            this.panel2 = new Panel();
            this.groupBox8 = new GroupBox();
            this.label11 = new Label();
            this.AnnounceRegex = new TextBox();
            this.label9 = new Label();
            this.label8 = new Label();
            this.Announcers = new TextBox();
            this.AnnounceChan = new TextBox();
            this.UseTLS = new CheckBox();
            this.label7 = new Label();
            this.IRCPort = new NumericUpDown();
            this.label6 = new Label();
            this.IRCServer = new TextBox();
            this.groupBox7 = new GroupBox();
            this.richTextBox1 = new RichTextBox();
            this.groupBox6 = new GroupBox();
            this.GoOnlineAtStartup = new CheckBox();
            this.label10 = new Label();
            this.SaveDebugLog = new CheckBox();
            this.DeleteLogDays = new NumericUpDown();
            this.DeleteLogsOlderThan = new CheckBox();
            this.label16 = new Label();
            this.FreeUnitOfMeasure = new ComboBox();
            this.FreeRequirement = new NumericUpDown();
            this.RequireAtLeast = new CheckBox();
            this.WarnOnQuitIfSettingsNotSaved = new CheckBox();
            this.RememberApplicationSizeAndPosition = new CheckBox();
            this.StartMinimized = new CheckBox();
            this.SaveSettingsOnQuit = new CheckBox();
            this.MinimizeToSystemTray = new CheckBox();
            this.BaseURL = new ComboBox();
            this.label14 = new Label();
            this.DoubleClickAction = new ComboBox();
            this.label5 = new Label();
            this.IgnoreInvalidTLSCertificates = new CheckBox();
            this.label4 = new Label();
            this.label15 = new Label();
            this.ThemeLabel = new Label();
            this.Theme = new ComboBox();
            this.UseRegexFilters = new CheckBox();
            this.SettingsToolbar = new ToolStrip();
            this.SaveSettingsButton1 = new ToolStripButton();
            this.toolStripSeparator3 = new ToolStripSeparator();
            this.RefreshLanguagesAndThemesButton = new ToolStripButton();
            this.DebugLevelText = new Label();
            this.DebugLevel = new NumericUpDown();
            this.Language = new ComboBox();
            this.LanguageLabel = new Label();
            this.DebugTab = new TabPage();
            this.DebugLog = new RichTextBox();
            this.groupBox5 = new GroupBox();
            this.richTextBox2 = new RichTextBox();
            this.notifyIcon1 = new NotifyIcon(this.components);
            this.IdleTimer = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new ToolTip(this.components);
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.DefaultActionToolbar.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.MainTabControl.SuspendLayout();
            this.AnnouncedTorrentsTab.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.FiltersTab.SuspendLayout();
            ((ISupportInitialize) this.Filters).BeginInit();
            this.RegexToolbox.SuspendLayout();
            this.FiltersToolbar.SuspendLayout();
            this.SettingsTab.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.IRCPort.BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.DeleteLogDays.BeginInit();
            this.FreeRequirement.BeginInit();
            this.SettingsToolbar.SuspendLayout();
            this.DebugLevel.BeginInit();
            this.DebugTab.SuspendLayout();
            this.groupBox5.SuspendLayout();
            base.SuspendLayout();
            ToolStripItem[] toolStripItems = new ToolStripItem[] { this.toolStripStatusLabel1, this.StatusLabel, this.toolStripStatusLabel4, this.TorrentsSeen, this.toolStripStatusLabel6, this.TorrentsActioned, this.toolStripStatusLabel2, this.WarningsOrErrors };
            this.statusStrip1.Items.AddRange(toolStripItems);
            this.statusStrip1.Location = new Point(0, 0x2b1);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new Size(0x52d, 0x18);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new Size(0x27, 0x13);
            this.toolStripStatusLabel1.Text = "Status";
            this.StatusLabel.BackColor = SystemColors.Control;
            this.StatusLabel.BorderSides = ToolStripStatusLabelBorderSides.All;
            this.StatusLabel.ForeColor = Color.Red;
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new Size(0x38, 0x13);
            this.StatusLabel.Text = "OFFLINE";
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new Size(0x4e, 0x13);
            this.toolStripStatusLabel4.Text = "Torrents Seen";
            this.TorrentsSeen.BorderSides = ToolStripStatusLabelBorderSides.All;
            this.TorrentsSeen.Name = "TorrentsSeen";
            this.TorrentsSeen.Size = new Size(0x11, 0x13);
            this.TorrentsSeen.Text = "0";
            this.toolStripStatusLabel6.Name = "toolStripStatusLabel6";
            this.toolStripStatusLabel6.Size = new Size(0x65, 0x13);
            this.toolStripStatusLabel6.Text = "Torrents Actioned";
            this.TorrentsActioned.BorderSides = ToolStripStatusLabelBorderSides.All;
            this.TorrentsActioned.Name = "TorrentsActioned";
            this.TorrentsActioned.Size = new Size(0x11, 0x13);
            this.TorrentsActioned.Text = "0";
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new Size(0x74, 0x13);
            this.toolStripStatusLabel2.Text = "Disk Space Warnings";
            this.WarningsOrErrors.BorderSides = ToolStripStatusLabelBorderSides.All;
            this.WarningsOrErrors.Name = "WarningsOrErrors";
            this.WarningsOrErrors.Size = new Size(40, 0x13);
            this.WarningsOrErrors.Text = "None";
            this.panel1.BackColor = SystemColors.Control;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = DockStyle.Top;
            this.panel1.Location = new Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new Size(0x52d, 0x53);
            this.panel1.TabIndex = 2;
            this.panel3.Controls.Add(this.DefaultActionToolbar);
            this.panel3.Controls.Add(this.DefaultAction);
            this.panel3.Controls.Add(this.DefaultActionLabel);
            this.panel3.Dock = DockStyle.Fill;
            this.panel3.Location = new Point(0x207, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new Size(0x326, 0x53);
            this.panel3.TabIndex = 2;
            this.DefaultActionToolbar.Dock = DockStyle.Bottom;
            this.DefaultActionToolbar.ImageScalingSize = new Size(0x18, 0x18);
            ToolStripItem[] itemArray2 = new ToolStripItem[] { this.FolderBrowseButton, this.Target };
            this.DefaultActionToolbar.Items.AddRange(itemArray2);
            this.DefaultActionToolbar.Location = new Point(0, 0x34);
            this.DefaultActionToolbar.Name = "DefaultActionToolbar";
            this.DefaultActionToolbar.Size = new Size(0x326, 0x1f);
            this.DefaultActionToolbar.TabIndex = 12;
            this.DefaultActionToolbar.Text = "toolStrip1";
            this.FolderBrowseButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.FolderBrowseButton.Image = (Image) manager.GetObject("FolderBrowseButton.Image");
            this.FolderBrowseButton.ImageTransparentColor = Color.Magenta;
            this.FolderBrowseButton.Name = "FolderBrowseButton";
            this.FolderBrowseButton.Size = new Size(0x1c, 0x1c);
            this.FolderBrowseButton.Text = "Browse for Folder";
            this.FolderBrowseButton.Click += new EventHandler(this.FolderBrowseButton_Click);
            this.Target.Font = new Font("Segoe UI", 9f);
            this.Target.Name = "Target";
            this.Target.Size = new Size(400, 0x1f);
            this.Target.ToolTipText = "Specify a local path, UNC Path or remote server path where the torrent will be uploaded.  Remote server paths supported: FTP, FTPS, SFTP/SCP.";
            this.DefaultAction.DropDownStyle = ComboBoxStyle.DropDownList;
            this.DefaultAction.FormattingEnabled = true;
            object[] items = new object[] { "Do Nothing", "Test", "Notify", "Open URL", "Save To Target", "Open in Torrent Client" };
            this.DefaultAction.Items.AddRange(items);
            this.DefaultAction.Location = new Point(0x56, 0x12);
            this.DefaultAction.Name = "DefaultAction";
            this.DefaultAction.Size = new Size(0x79, 0x15);
            this.DefaultAction.TabIndex = 11;
            this.DefaultActionLabel.AutoSize = true;
            this.DefaultActionLabel.Location = new Point(6, 0x15);
            this.DefaultActionLabel.Name = "DefaultActionLabel";
            this.DefaultActionLabel.Size = new Size(0x4a, 13);
            this.DefaultActionLabel.TabIndex = 10;
            this.DefaultActionLabel.Text = "Default Action";
            this.groupBox1.Controls.Add(this.GoOfflineButton);
            this.groupBox1.Controls.Add(this.GoOnlineButton);
            this.groupBox1.Controls.Add(this.RSSKey);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.BotNickname);
            this.groupBox1.Controls.Add(this.SiteUsername);
            this.groupBox1.Dock = DockStyle.Left;
            this.groupBox1.Location = new Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(0x207, 0x53);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Control ";
            this.GoOfflineButton.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.GoOfflineButton.Enabled = false;
            this.GoOfflineButton.ImageAlign = ContentAlignment.MiddleLeft;
            this.GoOfflineButton.ImageIndex = 6;
            this.GoOfflineButton.ImageList = this.imageList1;
            this.GoOfflineButton.Location = new Point(0x1a6, 0x2c);
            this.GoOfflineButton.Name = "GoOfflineButton";
            this.GoOfflineButton.Size = new Size(0x5b, 0x21);
            this.GoOfflineButton.TabIndex = 7;
            this.GoOfflineButton.Text = "Go Offline";
            this.GoOfflineButton.TextAlign = ContentAlignment.MiddleRight;
            this.GoOfflineButton.UseVisualStyleBackColor = true;
            this.GoOfflineButton.Click += new EventHandler(this.GoOfflineButton_Click);
            this.imageList1.ImageStream = (ImageListStreamer) manager.GetObject("imageList1.ImageStream");
            this.imageList1.TransparentColor = Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Actions-edit-delete-icon.png");
            this.imageList1.Images.SetKeyName(1, "Actions-dialog-ok-apply-icon.png");
            this.imageList1.Images.SetKeyName(2, "Action-remove-icon.png");
            this.imageList1.Images.SetKeyName(3, "Actions-list-add-icon.png");
            this.imageList1.Images.SetKeyName(4, "Actions-document-save-icon.png");
            this.imageList1.Images.SetKeyName(5, "62917-open-file-folder-icon.png");
            this.imageList1.Images.SetKeyName(6, "Status-user-offline-icon.png");
            this.imageList1.Images.SetKeyName(7, "Status-user-online-icon.png");
            this.imageList1.Images.SetKeyName(8, "Programming-Bug-2-icon.png");
            this.imageList1.Images.SetKeyName(9, "Categories-preferences-system-icon.png");
            this.imageList1.Images.SetKeyName(10, "Categories-applications-development-icon.png");
            this.imageList1.Images.SetKeyName(11, "Apps-preferences-system-windows-actions-icon.png");
            this.imageList1.Images.SetKeyName(12, "Apps-kblogger-icon.png");
            this.imageList1.Images.SetKeyName(13, "Actions-view-refresh-icon.png");
            this.imageList1.Images.SetKeyName(14, "Actions-view-filter-icon.png");
            this.imageList1.Images.SetKeyName(15, "Actions-irc-voice-icon.png");
            this.GoOnlineButton.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.GoOnlineButton.ImageAlign = ContentAlignment.MiddleLeft;
            this.GoOnlineButton.ImageIndex = 7;
            this.GoOnlineButton.ImageList = this.imageList1;
            this.GoOnlineButton.Location = new Point(0x142, 0x2c);
            this.GoOnlineButton.Name = "GoOnlineButton";
            this.GoOnlineButton.Size = new Size(0x5b, 0x21);
            this.GoOnlineButton.TabIndex = 6;
            this.GoOnlineButton.Text = "Go Online";
            this.GoOnlineButton.TextAlign = ContentAlignment.MiddleRight;
            this.GoOnlineButton.UseVisualStyleBackColor = true;
            this.GoOnlineButton.Click += new EventHandler(this.GoOnlineButton_Click);
            this.RSSKey.Location = new Point(0x58, 0x2c);
            this.RSSKey.Name = "RSSKey";
            this.RSSKey.Size = new Size(170, 20);
            this.RSSKey.TabIndex = 5;
            this.RSSKey.UseSystemPasswordChar = true;
            this.RSSKey.TextChanged += new EventHandler(this.RSSKey_TextChanged);
            this.label3.AutoSize = true;
            this.label3.Location = new Point(6, 0x2f);
            this.label3.Name = "label3";
            this.label3.Size = new Size(50, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "RSS Key";
            this.label2.AutoSize = true;
            this.label2.Location = new Point(0x108, 0x15);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x4a, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Bot Nickname";
            this.label1.AutoSize = true;
            this.label1.Location = new Point(6, 0x15);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x4c, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Site Username";
            this.BotNickname.Location = new Point(0x157, 0x12);
            this.BotNickname.Name = "BotNickname";
            this.BotNickname.ReadOnly = true;
            this.BotNickname.Size = new Size(170, 20);
            this.BotNickname.TabIndex = 1;
            this.SiteUsername.Location = new Point(0x58, 0x12);
            this.SiteUsername.Name = "SiteUsername";
            this.SiteUsername.Size = new Size(170, 20);
            this.SiteUsername.TabIndex = 0;
            this.SiteUsername.TextChanged += new EventHandler(this.SiteUsername_TextChanged);
            this.ircClient.Connected += new EventHandler(this.ircClient_Connected);
            this.ircClient.Closed += new EventHandler(this.ircClient_Closed);
            this.ircClient.GotIrcError += new EventHandler<IrcErrorEventArgs>(this.ircClient_GotIrcError);
            this.ircClient.GotMessage += new EventHandler<ChatMessageEventArgs>(this.ircClient_GotMessage);
            this.ircClient.GotWelcomeMessage += new EventHandler<SimpleMessageEventArgs>(this.ircClient_GotWelcomeMessage);
            this.MainTabControl.Controls.Add(this.AnnouncedTorrentsTab);
            this.MainTabControl.Controls.Add(this.FiltersTab);
            this.MainTabControl.Controls.Add(this.SettingsTab);
            this.MainTabControl.Controls.Add(this.DebugTab);
            this.MainTabControl.DisplayStyleProvider.BlendStyle = BlendStyle.Normal;
            this.MainTabControl.DisplayStyleProvider.BorderColorDisabled = SystemColors.ControlLight;
            this.MainTabControl.DisplayStyleProvider.BorderColorFocused = Color.FromArgb(0x7f, 0x9d, 0xb9);
            this.MainTabControl.DisplayStyleProvider.BorderColorHighlighted = SystemColors.ControlDark;
            this.MainTabControl.DisplayStyleProvider.BorderColorSelected = SystemColors.ControlDark;
            this.MainTabControl.DisplayStyleProvider.BorderColorUnselected = SystemColors.ControlDark;
            this.MainTabControl.DisplayStyleProvider.CloserButtonFillColorFocused = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.CloserButtonFillColorFocusedActive = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.CloserButtonFillColorHighlighted = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.CloserButtonFillColorHighlightedActive = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.CloserButtonFillColorSelected = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.CloserButtonFillColorSelectedActive = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.CloserButtonFillColorUnselected = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.CloserButtonOutlineColorFocused = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.CloserButtonOutlineColorFocusedActive = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.CloserButtonOutlineColorHighlighted = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.CloserButtonOutlineColorHighlightedActive = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.CloserButtonOutlineColorSelected = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.CloserButtonOutlineColorSelectedActive = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.CloserButtonOutlineColorUnselected = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.CloserColorFocused = SystemColors.ControlDark;
            this.MainTabControl.DisplayStyleProvider.CloserColorFocusedActive = SystemColors.ControlDark;
            this.MainTabControl.DisplayStyleProvider.CloserColorHighlighted = SystemColors.ControlDark;
            this.MainTabControl.DisplayStyleProvider.CloserColorHighlightedActive = SystemColors.ControlDark;
            this.MainTabControl.DisplayStyleProvider.CloserColorSelected = SystemColors.ControlDark;
            this.MainTabControl.DisplayStyleProvider.CloserColorSelectedActive = SystemColors.ControlDark;
            this.MainTabControl.DisplayStyleProvider.CloserColorUnselected = Color.Empty;
            this.MainTabControl.DisplayStyleProvider.FocusTrack = false;
            this.MainTabControl.DisplayStyleProvider.HotTrack = true;
            this.MainTabControl.DisplayStyleProvider.ImageAlign = ContentAlignment.MiddleLeft;
            this.MainTabControl.DisplayStyleProvider.Opacity = 1f;
            this.MainTabControl.DisplayStyleProvider.Overlap = 0;
            this.MainTabControl.DisplayStyleProvider.Padding = new Point(6, 3);
            this.MainTabControl.DisplayStyleProvider.PageBackgroundColorDisabled = SystemColors.Control;
            this.MainTabControl.DisplayStyleProvider.PageBackgroundColorFocused = SystemColors.ControlLight;
            this.MainTabControl.DisplayStyleProvider.PageBackgroundColorHighlighted = Color.FromArgb(0xec, 0xf4, 0xfc);
            this.MainTabControl.DisplayStyleProvider.PageBackgroundColorSelected = SystemColors.ControlLightLight;
            this.MainTabControl.DisplayStyleProvider.PageBackgroundColorUnselected = SystemColors.Control;
            this.MainTabControl.DisplayStyleProvider.Radius = 2;
            this.MainTabControl.DisplayStyleProvider.SelectedTabIsLarger = true;
            this.MainTabControl.DisplayStyleProvider.ShowTabCloser = false;
            this.MainTabControl.DisplayStyleProvider.TabColorDisabled1 = SystemColors.Control;
            this.MainTabControl.DisplayStyleProvider.TabColorDisabled2 = SystemColors.Control;
            this.MainTabControl.DisplayStyleProvider.TabColorFocused1 = SystemColors.ControlLight;
            this.MainTabControl.DisplayStyleProvider.TabColorFocused2 = SystemColors.ControlLight;
            this.MainTabControl.DisplayStyleProvider.TabColorHighLighted1 = Color.FromArgb(0xec, 0xf4, 0xfc);
            this.MainTabControl.DisplayStyleProvider.TabColorHighLighted2 = Color.FromArgb(0xdd, 0xed, 0xfc);
            this.MainTabControl.DisplayStyleProvider.TabColorSelected1 = SystemColors.ControlLightLight;
            this.MainTabControl.DisplayStyleProvider.TabColorSelected2 = SystemColors.ControlLightLight;
            this.MainTabControl.DisplayStyleProvider.TabColorUnSelected1 = SystemColors.Control;
            this.MainTabControl.DisplayStyleProvider.TabColorUnSelected2 = SystemColors.Control;
            this.MainTabControl.DisplayStyleProvider.TabPageMargin = new Padding(1);
            this.MainTabControl.DisplayStyleProvider.TextColorDisabled = SystemColors.ControlDark;
            this.MainTabControl.DisplayStyleProvider.TextColorFocused = SystemColors.ControlText;
            this.MainTabControl.DisplayStyleProvider.TextColorHighlighted = SystemColors.ControlText;
            this.MainTabControl.DisplayStyleProvider.TextColorSelected = SystemColors.ControlText;
            this.MainTabControl.DisplayStyleProvider.TextColorUnselected = SystemColors.ControlText;
            this.MainTabControl.Dock = DockStyle.Fill;
            this.MainTabControl.HotTrack = true;
            this.MainTabControl.ImageList = this.imageList1;
            this.MainTabControl.ItemSize = new Size(0xb1, 30);
            this.MainTabControl.Location = new Point(0, 0x53);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedIndex = 0;
            this.MainTabControl.ShowToolTips = true;
            this.MainTabControl.Size = new Size(0x52d, 0x25e);
            this.MainTabControl.TabIndex = 4;
            this.AnnouncedTorrentsTab.Controls.Add(this.TorrentList);
            this.AnnouncedTorrentsTab.ImageIndex = 12;
            this.AnnouncedTorrentsTab.Location = new Point(4, 0x23);
            this.AnnouncedTorrentsTab.Name = "AnnouncedTorrentsTab";
            this.AnnouncedTorrentsTab.Padding = new Padding(3);
            this.AnnouncedTorrentsTab.Size = new Size(0x525, 0x237);
            this.AnnouncedTorrentsTab.TabIndex = 0;
            this.AnnouncedTorrentsTab.Text = "Announced Torrents";
            this.AnnouncedTorrentsTab.ToolTipText = "The Announced Torrents list will show you all releases which have been announced - whether or not they have matched a filter.";
            this.AnnouncedTorrentsTab.UseVisualStyleBackColor = true;
            ColumnHeader[] values = new ColumnHeader[] { this.columnHeader1, this.columnHeader2, this.columnHeader3, this.columnHeader4, this.columnHeader5, this.columnHeader6 };
            this.TorrentList.Columns.AddRange(values);
            this.TorrentList.ContextMenuStrip = this.contextMenuStrip1;
            this.TorrentList.Dock = DockStyle.Fill;
            this.TorrentList.FullRowSelect = true;
            this.TorrentList.HideSelection = false;
            this.TorrentList.LabelEdit = true;
            this.TorrentList.Location = new Point(3, 3);
            this.TorrentList.Name = "TorrentList";
            this.TorrentList.Size = new Size(0x51f, 0x231);
            this.TorrentList.SmallImageList = this.imageList1;
            this.TorrentList.TabIndex = 1;
            this.TorrentList.UseCompatibleStateImageBehavior = false;
            this.TorrentList.View = View.Details;
            this.TorrentList.DoubleClick += new EventHandler(this.TorrentList_DoubleClick_1);
            this.columnHeader1.Text = "Date & Time";
            this.columnHeader1.Width = 150;
            this.columnHeader2.Text = "Category";
            this.columnHeader2.Width = 150;
            this.columnHeader3.Text = "Name";
            this.columnHeader3.Width = 360;
            this.columnHeader4.Text = "Uploader";
            this.columnHeader4.Width = 150;
            this.columnHeader5.Text = "Free";
            this.columnHeader6.Text = "Torrent ID";
            ToolStripItem[] itemArray3 = new ToolStripItem[12];
            itemArray3[0] = this.ActionDefault;
            itemArray3[1] = this.ActionSelectAll;
            itemArray3[2] = this.toolStripSeparator4;
            itemArray3[3] = this.ActionRemove;
            itemArray3[4] = this.ActionRemoveUnmatched;
            itemArray3[5] = this.ActionRemoveNegatedReleases;
            itemArray3[6] = this.ActionRemoveAll;
            itemArray3[7] = this.toolStripSeparator5;
            itemArray3[8] = this.ActionTest;
            itemArray3[9] = this.ActionOpenURL;
            itemArray3[10] = this.ActionSaveToTarget;
            itemArray3[11] = this.ActionOpenInTorrentClient;
            this.contextMenuStrip1.Items.AddRange(itemArray3);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new Size(0x10c, 0xec);
            this.contextMenuStrip1.Closed += new ToolStripDropDownClosedEventHandler(this.contextMenuStrip1_Closed);
            this.contextMenuStrip1.Opening += new CancelEventHandler(this.contextMenuStrip1_Opening);
            this.ActionDefault.DoubleClickEnabled = true;
            this.ActionDefault.Font = new Font("Segoe UI", 9f, FontStyle.Bold);
            this.ActionDefault.Name = "ActionDefault";
            this.ActionDefault.Size = new Size(0x10b, 0x16);
            this.ActionDefault.Click += new EventHandler(this.ActionDefault_Click);
            this.ActionSelectAll.Name = "ActionSelectAll";
            this.ActionSelectAll.ShortcutKeys = Keys.Control | Keys.A;
            this.ActionSelectAll.Size = new Size(0x10b, 0x16);
            this.ActionSelectAll.Text = "Select All";
            this.ActionSelectAll.Click += new EventHandler(this.ActionSelectAll_Click);
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new Size(0x108, 6);
            this.ActionRemove.Name = "ActionRemove";
            this.ActionRemove.ShortcutKeys = Keys.Delete;
            this.ActionRemove.Size = new Size(0x10b, 0x16);
            this.ActionRemove.Text = "Remove";
            this.ActionRemove.ToolTipText = "Remove Selected Releases from view";
            this.ActionRemove.Click += new EventHandler(this.ActionRemove_Click);
            this.ActionRemoveUnmatched.Name = "ActionRemoveUnmatched";
            this.ActionRemoveUnmatched.ShortcutKeys = Keys.Control | Keys.U;
            this.ActionRemoveUnmatched.Size = new Size(0x10b, 0x16);
            this.ActionRemoveUnmatched.Text = "Remove unmatched releases";
            this.ActionRemoveUnmatched.Click += new EventHandler(this.ActionRemoveUnmatched_Click);
            this.ActionRemoveNegatedReleases.Name = "ActionRemoveNegatedReleases";
            this.ActionRemoveNegatedReleases.ShortcutKeys = Keys.Control | Keys.N;
            this.ActionRemoveNegatedReleases.Size = new Size(0x10b, 0x16);
            this.ActionRemoveNegatedReleases.Text = "Remove negated releases";
            this.ActionRemoveNegatedReleases.Click += new EventHandler(this.ActionRemoveNegatedReleases_Click);
            this.ActionRemoveAll.Name = "ActionRemoveAll";
            this.ActionRemoveAll.ShortcutKeys = Keys.Control | Keys.Delete;
            this.ActionRemoveAll.Size = new Size(0x10b, 0x16);
            this.ActionRemoveAll.Text = "Remove All";
            this.ActionRemoveAll.Click += new EventHandler(this.ActionRemoveAll_Click);
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new Size(0x108, 6);
            this.ActionTest.Name = "ActionTest";
            this.ActionTest.ShortcutKeys = Keys.Control | Keys.T;
            this.ActionTest.Size = new Size(0x10b, 0x16);
            this.ActionTest.Text = "Test";
            this.ActionTest.Click += new EventHandler(this.ActionDefault_Click);
            this.ActionOpenURL.Name = "ActionOpenURL";
            this.ActionOpenURL.ShortcutKeys = Keys.Control | Keys.O;
            this.ActionOpenURL.Size = new Size(0x10b, 0x16);
            this.ActionOpenURL.Text = "Open URL";
            this.ActionOpenURL.Click += new EventHandler(this.ActionDefault_Click);
            this.ActionSaveToTarget.Name = "ActionSaveToTarget";
            this.ActionSaveToTarget.ShortcutKeys = Keys.Control | Keys.S;
            this.ActionSaveToTarget.Size = new Size(0x10b, 0x16);
            this.ActionSaveToTarget.Text = "Save To Target";
            this.ActionSaveToTarget.Click += new EventHandler(this.ActionDefault_Click);
            this.ActionOpenInTorrentClient.Name = "ActionOpenInTorrentClient";
            this.ActionOpenInTorrentClient.ShortcutKeys = Keys.Alt | Keys.O;
            this.ActionOpenInTorrentClient.Size = new Size(0x10b, 0x16);
            this.ActionOpenInTorrentClient.Text = "Open in Torrent Client";
            this.ActionOpenInTorrentClient.Click += new EventHandler(this.ActionDefault_Click);
            this.FiltersTab.Controls.Add(this.Filters);
            this.FiltersTab.Controls.Add(this.FilterHelp);
            this.FiltersTab.Controls.Add(this.RegexToolbox);
            this.FiltersTab.Controls.Add(this.FiltersToolbar);
            this.FiltersTab.ImageIndex = 14;
            this.FiltersTab.Location = new Point(4, 0x23);
            this.FiltersTab.Name = "FiltersTab";
            this.FiltersTab.Padding = new Padding(3);
            this.FiltersTab.Size = new Size(0x525, 0x237);
            this.FiltersTab.TabIndex = 1;
            this.FiltersTab.Text = "Filters";
            this.FiltersTab.ToolTipText = "This tab allows you to add rules where you can choose actions based on keyword matches for releases which are announced";
            this.FiltersTab.UseVisualStyleBackColor = true;
            this.Filters.AllowDrop = true;
            this.Filters.AllowUserToAddRows = false;
            style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            style.BackColor = SystemColors.Control;
            style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            style.ForeColor = SystemColors.WindowText;
            style.SelectionBackColor = SystemColors.Highlight;
            style.SelectionForeColor = SystemColors.HighlightText;
            style.WrapMode = DataGridViewTriState.True;
            this.Filters.ColumnHeadersDefaultCellStyle = style;
            this.Filters.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            DataGridViewColumn[] dataGridViewColumns = new DataGridViewColumn[10];
            dataGridViewColumns[0] = this.FilterEnabled;
            dataGridViewColumns[1] = this.FilterFreeleech;
            dataGridViewColumns[2] = this.FilterTitle;
            dataGridViewColumns[3] = this.FilterCategory;
            dataGridViewColumns[4] = this.FilterExcludeTitle;
            dataGridViewColumns[5] = this.FilterExcludeCategory;
            dataGridViewColumns[6] = this.FilterMinSize;
            dataGridViewColumns[7] = this.FilterMaxSize;
            dataGridViewColumns[8] = this.Action;
            dataGridViewColumns[9] = this.IndividualTarget;
            this.Filters.Columns.AddRange(dataGridViewColumns);
            style2.Alignment = DataGridViewContentAlignment.MiddleLeft;
            style2.BackColor = SystemColors.Window;
            style2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            style2.ForeColor = SystemColors.ControlText;
            style2.SelectionBackColor = SystemColors.Highlight;
            style2.SelectionForeColor = SystemColors.HighlightText;
            style2.WrapMode = DataGridViewTriState.False;
            this.Filters.DefaultCellStyle = style2;
            this.Filters.Dock = DockStyle.Fill;
            this.Filters.Location = new Point(3, 0x9b);
            this.Filters.Name = "Filters";
            this.Filters.Size = new Size(0x51f, 340);
            this.Filters.TabIndex = 12;
            this.Filters.CurrentCellDirtyStateChanged += new EventHandler(this.Filters_CurrentCellDirtyStateChanged);
            this.Filters.DragDrop += new DragEventHandler(this.Filters_DragDrop);
            this.Filters.DragEnter += new DragEventHandler(this.Filters_DragEnter);
            this.Filters.DragOver += new DragEventHandler(this.Filters_DragOver);
            this.Filters.MouseClick += new MouseEventHandler(this.Filters_MouseClick);
            this.Filters.MouseDown += new MouseEventHandler(this.Filters_MouseDown);
            this.Filters.MouseMove += new MouseEventHandler(this.Filters_MouseMove);
            this.FilterEnabled.HeaderText = "Enabled";
            this.FilterEnabled.Name = "FilterEnabled";
            this.FilterEnabled.ToolTipText = "For filters to be effective, they have to be enabled.  Check the box next to the filter to enable it.";
            this.FilterEnabled.Width = 60;
            this.FilterFreeleech.HeaderText = "Freeleech";
            object[] objArray2 = new object[] { "Don't Care", "Yes", "No" };
            this.FilterFreeleech.Items.AddRange(objArray2);
            this.FilterFreeleech.Name = "FilterFreeleech";
            this.FilterFreeleech.ToolTipText = "Here, you specify whether or not the release should be Freeleech or not, or if you don't care if it is or isn't";
            this.FilterTitle.HeaderText = "Title";
            this.FilterTitle.Name = "FilterTitle";
            this.FilterTitle.ToolTipText = "The Title (or Name) of the release you would like to match.  If for example you wish to match all 720p content, you would specify *720p*";
            this.FilterTitle.Width = 120;
            this.FilterCategory.HeaderText = "Category";
            this.FilterCategory.Name = "FilterCategory";
            this.FilterCategory.ToolTipText = "This defines which category the release should be in to match.  If you wish for example to say that it should be Movies, use Movies* as your filter";
            this.FilterCategory.Width = 120;
            this.FilterExcludeTitle.HeaderText = "Exclude Title";
            this.FilterExcludeTitle.Name = "FilterExcludeTitle";
            this.FilterExcludeTitle.ToolTipText = "Use this to optionally exclude items from the title match.  For example, if you want to EXCLUDE all -m0nkrus releases, use *-m0nkrus";
            this.FilterExcludeTitle.Width = 120;
            this.FilterExcludeCategory.HeaderText = "Exclude Category";
            this.FilterExcludeCategory.Name = "FilterExcludeCategory";
            this.FilterExcludeCategory.ToolTipText = "If you want to exclude any specific categories, specify them here.  For example, if you want to exclude all TV Shows, use TV*";
            this.FilterExcludeCategory.Width = 120;
            this.FilterMinSize.HeaderText = "Min Size";
            this.FilterMinSize.Name = "FilterMinSize";
            this.FilterMinSize.ToolTipText = "If you want to specify a minum size for a match, use this here.  You can specify a number, or M, G and T for big numbers.  For example, if it should be at least 1 Gigabyte, use 1G";
            this.FilterMaxSize.HeaderText = "MaxSize";
            this.FilterMaxSize.Name = "FilterMaxSize";
            this.FilterMaxSize.ToolTipText = "If you want to specify a maximum size for a match, use this here.  You can specify a number, or M, G and T for big numbers.  For example, if it should be at most 10 Gigabytes, use 10G";
            this.Action.HeaderText = "Action";
            object[] objArray3 = new object[] { "Do Nothing", "Test", "Notify", "Open URL", "Save To Target", "Open in Torrent Client", "Use Default" };
            this.Action.Items.AddRange(objArray3);
            this.Action.Name = "Action";
            this.Action.ToolTipText = "Here you choose an action for any releases that match this filter rule.  \"Use Default\" would use the default action shown at the top of this app.";
            this.IndividualTarget.HeaderText = "Target";
            this.IndividualTarget.Name = "IndividualTarget";
            this.IndividualTarget.ToolTipText = "If you choose to \"Save to Target\", you specify the target path or URL here.";
            this.IndividualTarget.Width = 300;
            this.FilterHelp.Dock = DockStyle.Top;
            this.FilterHelp.Location = new Point(3, 0x22);
            this.FilterHelp.Name = "FilterHelp";
            this.FilterHelp.ReadOnly = true;
            this.FilterHelp.Size = new Size(0x51f, 0x79);
            this.FilterHelp.TabIndex = 11;
            this.FilterHelp.Text = manager.GetString("FilterHelp.Text");
            this.RegexToolbox.Controls.Add(this.MatchOrNoMatch);
            this.RegexToolbox.Controls.Add(this.label13);
            this.RegexToolbox.Controls.Add(this.label12);
            this.RegexToolbox.Controls.Add(this.TestString);
            this.RegexToolbox.Controls.Add(this.RegularExpression);
            this.RegexToolbox.Dock = DockStyle.Bottom;
            this.RegexToolbox.Location = new Point(3, 0x1ef);
            this.RegexToolbox.Name = "RegexToolbox";
            this.RegexToolbox.Size = new Size(0x51f, 0x45);
            this.RegexToolbox.TabIndex = 8;
            this.RegexToolbox.TabStop = false;
            this.RegexToolbox.Text = " Regex Testing Toolbox ";
            this.RegexToolbox.Visible = false;
            this.MatchOrNoMatch.AutoSize = true;
            this.MatchOrNoMatch.Location = new Point(0x14d, 0x16);
            this.MatchOrNoMatch.Name = "MatchOrNoMatch";
            this.MatchOrNoMatch.Size = new Size(0x49, 13);
            this.MatchOrNoMatch.TabIndex = 4;
            this.MatchOrNoMatch.Text = "Start Typing :)";
            this.label13.AutoSize = true;
            this.label13.Location = new Point(6, 0x30);
            this.label13.Name = "label13";
            this.label13.Size = new Size(0x3a, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "Test String";
            this.label12.AutoSize = true;
            this.label12.Location = new Point(6, 0x16);
            this.label12.Name = "label12";
            this.label12.Size = new Size(0x62, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Regular Expression";
            this.TestString.Location = new Point(110, 0x2d);
            this.TestString.Name = "TestString";
            this.TestString.Size = new Size(0x234, 20);
            this.TestString.TabIndex = 1;
            this.TestString.TextChanged += new EventHandler(this.RegularExpression_TextChanged);
            this.RegularExpression.Location = new Point(110, 0x13);
            this.RegularExpression.Name = "RegularExpression";
            this.RegularExpression.Size = new Size(200, 20);
            this.RegularExpression.TabIndex = 0;
            this.RegularExpression.TextChanged += new EventHandler(this.RegularExpression_TextChanged);
            this.FiltersToolbar.ImageScalingSize = new Size(0x18, 0x18);
            ToolStripItem[] itemArray4 = new ToolStripItem[] { this.AddFilterButton, this.toolStripSeparator1, this.ToggleFilterHelpButton, this.toolStripSeparator2, this.SaveSettingsButton2 };
            this.FiltersToolbar.Items.AddRange(itemArray4);
            this.FiltersToolbar.Location = new Point(3, 3);
            this.FiltersToolbar.Name = "FiltersToolbar";
            this.FiltersToolbar.Size = new Size(0x51f, 0x1f);
            this.FiltersToolbar.TabIndex = 6;
            this.FiltersToolbar.Text = "Save Settings";
            this.AddFilterButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.AddFilterButton.Image = (Image) manager.GetObject("AddFilterButton.Image");
            this.AddFilterButton.ImageTransparentColor = Color.Magenta;
            this.AddFilterButton.Name = "AddFilterButton";
            this.AddFilterButton.Size = new Size(0x1c, 0x1c);
            this.AddFilterButton.Text = "Add Filter";
            this.AddFilterButton.Click += new EventHandler(this.AddFilterButton_Click);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new Size(6, 0x1f);
            this.ToggleFilterHelpButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.ToggleFilterHelpButton.Image = (Image) manager.GetObject("ToggleFilterHelpButton.Image");
            this.ToggleFilterHelpButton.ImageTransparentColor = Color.Magenta;
            this.ToggleFilterHelpButton.Name = "ToggleFilterHelpButton";
            this.ToggleFilterHelpButton.Size = new Size(0x1c, 0x1c);
            this.ToggleFilterHelpButton.Text = "Show / Hide Filter Help Information ";
            this.ToggleFilterHelpButton.Click += new EventHandler(this.ToggleFilterHelpButton_Click);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new Size(6, 0x1f);
            this.SaveSettingsButton2.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.SaveSettingsButton2.Image = (Image) manager.GetObject("SaveSettingsButton2.Image");
            this.SaveSettingsButton2.ImageTransparentColor = Color.Magenta;
            this.SaveSettingsButton2.Name = "SaveSettingsButton2";
            this.SaveSettingsButton2.Size = new Size(0x1c, 0x1c);
            this.SaveSettingsButton2.Text = "Save Settings and Filters";
            this.SaveSettingsButton2.Click += new EventHandler(this.SaveSettingsButton2_Click);
            this.SettingsTab.Controls.Add(this.panel2);
            this.SettingsTab.Controls.Add(this.groupBox6);
            this.SettingsTab.ImageIndex = 9;
            this.SettingsTab.Location = new Point(4, 0x23);
            this.SettingsTab.Name = "SettingsTab";
            this.SettingsTab.Padding = new Padding(3);
            this.SettingsTab.Size = new Size(0x525, 0x237);
            this.SettingsTab.TabIndex = 2;
            this.SettingsTab.Text = "Settings";
            this.SettingsTab.ToolTipText = "Here you can change settings of the application.";
            this.SettingsTab.UseVisualStyleBackColor = true;
            this.panel2.Controls.Add(this.groupBox8);
            this.panel2.Controls.Add(this.groupBox7);
            this.panel2.Dock = DockStyle.Fill;
            this.panel2.Location = new Point(0x1a0, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new Size(0x382, 0x231);
            this.panel2.TabIndex = 2;
            this.groupBox8.Controls.Add(this.label11);
            this.groupBox8.Controls.Add(this.AnnounceRegex);
            this.groupBox8.Controls.Add(this.label9);
            this.groupBox8.Controls.Add(this.label8);
            this.groupBox8.Controls.Add(this.Announcers);
            this.groupBox8.Controls.Add(this.AnnounceChan);
            this.groupBox8.Controls.Add(this.UseTLS);
            this.groupBox8.Controls.Add(this.label7);
            this.groupBox8.Controls.Add(this.IRCPort);
            this.groupBox8.Controls.Add(this.label6);
            this.groupBox8.Controls.Add(this.IRCServer);
            this.groupBox8.Dock = DockStyle.Top;
            this.groupBox8.Location = new Point(0, 0x9f);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new Size(0x382, 0x7e);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = " Application Variables ";
            this.groupBox8.Visible = false;
            this.label11.AutoSize = true;
            this.label11.Location = new Point(6, 0x61);
            this.label11.Name = "label11";
            this.label11.Size = new Size(90, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Announce Regex";
            this.label11.TextAlign = ContentAlignment.TopCenter;
            this.AnnounceRegex.Location = new Point(0x6d, 0x5e);
            this.AnnounceRegex.Name = "AnnounceRegex";
            this.AnnounceRegex.ReadOnly = true;
            this.AnnounceRegex.Size = new Size(0x181, 20);
            this.AnnounceRegex.TabIndex = 11;
            this.AnnounceRegex.Text = @"New Torrent Announcement:\s*<([^>]*)>\s*Name:'(.*)' uploaded by '([^']*)'\s*(freeleech)*\s*-\s*https?\:\/\/([^\/]+\/)torrent\/(\d+)";
            this.label9.AutoSize = true;
            this.label9.Location = new Point(6, 0x47);
            this.label9.Name = "label9";
            this.label9.Size = new Size(0x40, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Announcers";
            this.label9.TextAlign = ContentAlignment.TopCenter;
            this.label8.AutoSize = true;
            this.label8.Location = new Point(6, 0x30);
            this.label8.Name = "label8";
            this.label8.Size = new Size(0x62, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Announce Channel";
            this.label8.TextAlign = ContentAlignment.TopCenter;
            this.Announcers.Location = new Point(0x70, 0x44);
            this.Announcers.Name = "Announcers";
            this.Announcers.ReadOnly = true;
            this.Announcers.Size = new Size(0x17e, 20);
            this.Announcers.TabIndex = 8;
            this.Announcers.Text = "_AnnounceBot_,TORRENTLEECH,TL-Monkey";
            this.AnnounceChan.Location = new Point(0x70, 0x2d);
            this.AnnounceChan.Name = "AnnounceChan";
            this.AnnounceChan.ReadOnly = true;
            this.AnnounceChan.Size = new Size(170, 20);
            this.AnnounceChan.TabIndex = 7;
            this.AnnounceChan.Text = "#tlannounces";
            this.UseTLS.AutoSize = true;
            this.UseTLS.Checked = true;
            this.UseTLS.CheckState = CheckState.Checked;
            this.UseTLS.Enabled = false;
            this.UseTLS.Location = new Point(0x1aa, 0x16);
            this.UseTLS.Name = "UseTLS";
            this.UseTLS.Size = new Size(0x44, 0x11);
            this.UseTLS.TabIndex = 6;
            this.UseTLS.Text = "Use TLS";
            this.UseTLS.UseVisualStyleBackColor = true;
            this.label7.AutoSize = true;
            this.label7.Location = new Point(0x120, 0x16);
            this.label7.Name = "label7";
            this.label7.Size = new Size(0x2f, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "IRC Port";
            this.IRCPort.Location = new Point(0x155, 20);
            int[] bits = new int[4];
            bits[0] = 0xffff;
            this.IRCPort.Maximum = new decimal(bits);
            int[] numArray2 = new int[4];
            numArray2[0] = 1;
            this.IRCPort.Minimum = new decimal(numArray2);
            this.IRCPort.Name = "IRCPort";
            this.IRCPort.ReadOnly = true;
            this.IRCPort.Size = new Size(0x4f, 20);
            this.IRCPort.TabIndex = 4;
            int[] numArray3 = new int[4];
            numArray3[0] = 0x1b6d;
            this.IRCPort.Value = new decimal(numArray3);
            this.label6.AutoSize = true;
            this.label6.Location = new Point(6, 0x16);
            this.label6.Name = "label6";
            this.label6.Size = new Size(0x3b, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "IRC Server";
            this.IRCServer.Location = new Point(0x70, 0x13);
            this.IRCServer.Name = "IRCServer";
            this.IRCServer.ReadOnly = true;
            this.IRCServer.Size = new Size(170, 20);
            this.IRCServer.TabIndex = 1;
            this.IRCServer.Text = "irc.torrentleech.org";
            this.groupBox7.Controls.Add(this.richTextBox1);
            this.groupBox7.Dock = DockStyle.Top;
            this.groupBox7.Location = new Point(0, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new Size(0x382, 0x9f);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = " Debug Levels ";
            this.richTextBox1.Dock = DockStyle.Fill;
            this.richTextBox1.Location = new Point(3, 0x10);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new Size(0x37c, 140);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = manager.GetString("richTextBox1.Text");
            this.groupBox6.Controls.Add(this.GoOnlineAtStartup);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.SaveDebugLog);
            this.groupBox6.Controls.Add(this.DeleteLogDays);
            this.groupBox6.Controls.Add(this.DeleteLogsOlderThan);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.FreeUnitOfMeasure);
            this.groupBox6.Controls.Add(this.FreeRequirement);
            this.groupBox6.Controls.Add(this.RequireAtLeast);
            this.groupBox6.Controls.Add(this.WarnOnQuitIfSettingsNotSaved);
            this.groupBox6.Controls.Add(this.RememberApplicationSizeAndPosition);
            this.groupBox6.Controls.Add(this.StartMinimized);
            this.groupBox6.Controls.Add(this.SaveSettingsOnQuit);
            this.groupBox6.Controls.Add(this.MinimizeToSystemTray);
            this.groupBox6.Controls.Add(this.BaseURL);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.DoubleClickAction);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.IgnoreInvalidTLSCertificates);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.ThemeLabel);
            this.groupBox6.Controls.Add(this.Theme);
            this.groupBox6.Controls.Add(this.UseRegexFilters);
            this.groupBox6.Controls.Add(this.SettingsToolbar);
            this.groupBox6.Controls.Add(this.DebugLevelText);
            this.groupBox6.Controls.Add(this.DebugLevel);
            this.groupBox6.Controls.Add(this.Language);
            this.groupBox6.Controls.Add(this.LanguageLabel);
            this.groupBox6.Dock = DockStyle.Left;
            this.groupBox6.Location = new Point(3, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new Size(0x19d, 0x231);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = " Preferences ";
            this.GoOnlineAtStartup.AutoSize = true;
            this.GoOnlineAtStartup.Location = new Point(6, 0xb0);
            this.GoOnlineAtStartup.Name = "GoOnlineAtStartup";
            this.GoOnlineAtStartup.Size = new Size(0x7b, 0x11);
            this.GoOnlineAtStartup.TabIndex = 0x20;
            this.GoOnlineAtStartup.Text = "Go Online At Startup";
            this.GoOnlineAtStartup.UseVisualStyleBackColor = true;
            this.GoOnlineAtStartup.Click += new EventHandler(this.SettingsChangeClick);
            this.label10.AutoSize = true;
            this.label10.Location = new Point(0xda, 360);
            this.label10.Name = "label10";
            this.label10.Size = new Size(0x1f, 13);
            this.label10.TabIndex = 0x1f;
            this.label10.Text = "Days";
            this.SaveDebugLog.AutoSize = true;
            this.SaveDebugLog.Location = new Point(6, 0x150);
            this.SaveDebugLog.Name = "SaveDebugLog";
            this.SaveDebugLog.Size = new Size(0x6b, 0x11);
            this.SaveDebugLog.TabIndex = 30;
            this.SaveDebugLog.Text = "Save Debug Log";
            this.SaveDebugLog.UseVisualStyleBackColor = true;
            this.SaveDebugLog.Click += new EventHandler(this.SettingsChangeClick);
            this.DeleteLogDays.Location = new Point(0x8a, 0x166);
            int[] numArray4 = new int[4];
            numArray4[0] = 90;
            this.DeleteLogDays.Maximum = new decimal(numArray4);
            int[] numArray5 = new int[4];
            numArray5[0] = 1;
            this.DeleteLogDays.Minimum = new decimal(numArray5);
            this.DeleteLogDays.Name = "DeleteLogDays";
            this.DeleteLogDays.Size = new Size(0x4a, 20);
            this.DeleteLogDays.TabIndex = 0x1d;
            int[] numArray6 = new int[4];
            numArray6[0] = 30;
            this.DeleteLogDays.Value = new decimal(numArray6);
            this.DeleteLogDays.Click += new EventHandler(this.SettingsChangeClick);
            this.DeleteLogsOlderThan.AutoSize = true;
            this.DeleteLogsOlderThan.Location = new Point(6, 0x167);
            this.DeleteLogsOlderThan.Name = "DeleteLogsOlderThan";
            this.DeleteLogsOlderThan.Size = new Size(0x81, 0x11);
            this.DeleteLogsOlderThan.TabIndex = 0x1c;
            this.DeleteLogsOlderThan.Text = "Delete logs older than";
            this.DeleteLogsOlderThan.UseVisualStyleBackColor = true;
            this.DeleteLogsOlderThan.Click += new EventHandler(this.SettingsChangeClick);
            this.label16.AutoSize = true;
            this.label16.Location = new Point(0x14f, 0x13d);
            this.label16.Name = "label16";
            this.label16.Size = new Size(0x1c, 13);
            this.label16.TabIndex = 0x1b;
            this.label16.Text = "Free";
            this.FreeUnitOfMeasure.DropDownStyle = ComboBoxStyle.DropDownList;
            this.FreeUnitOfMeasure.FormattingEnabled = true;
            object[] objArray4 = new object[] { "Gigabytes (GB)", "Terabytes (TB)" };
            this.FreeUnitOfMeasure.Items.AddRange(objArray4);
            this.FreeUnitOfMeasure.Location = new Point(0xd9, 0x13a);
            this.FreeUnitOfMeasure.Name = "FreeUnitOfMeasure";
            this.FreeUnitOfMeasure.Size = new Size(0x70, 0x15);
            this.FreeUnitOfMeasure.TabIndex = 0x1a;
            this.FreeUnitOfMeasure.Click += new EventHandler(this.SettingsChangeClick);
            int[] numArray7 = new int[4];
            numArray7[0] = 10;
            this.FreeRequirement.Increment = new decimal(numArray7);
            this.FreeRequirement.Location = new Point(0x89, 0x13b);
            int[] numArray8 = new int[4];
            numArray8[0] = 0x3e8;
            this.FreeRequirement.Maximum = new decimal(numArray8);
            int[] numArray9 = new int[4];
            numArray9[0] = 1;
            this.FreeRequirement.Minimum = new decimal(numArray9);
            this.FreeRequirement.Name = "FreeRequirement";
            this.FreeRequirement.Size = new Size(0x4a, 20);
            this.FreeRequirement.TabIndex = 0x19;
            int[] numArray10 = new int[4];
            numArray10[0] = 50;
            this.FreeRequirement.Value = new decimal(numArray10);
            this.FreeRequirement.Click += new EventHandler(this.SettingsChangeClick);
            this.RequireAtLeast.AutoSize = true;
            this.RequireAtLeast.Location = new Point(6, 0x139);
            this.RequireAtLeast.Name = "RequireAtLeast";
            this.RequireAtLeast.Size = new Size(0x68, 0x11);
            this.RequireAtLeast.TabIndex = 0x18;
            this.RequireAtLeast.Text = "Require at Least";
            this.RequireAtLeast.UseVisualStyleBackColor = true;
            this.RequireAtLeast.Click += new EventHandler(this.SettingsChangeClick);
            this.WarnOnQuitIfSettingsNotSaved.AutoSize = true;
            this.WarnOnQuitIfSettingsNotSaved.Checked = true;
            this.WarnOnQuitIfSettingsNotSaved.CheckState = CheckState.Checked;
            this.WarnOnQuitIfSettingsNotSaved.Location = new Point(6, 0x123);
            this.WarnOnQuitIfSettingsNotSaved.Name = "WarnOnQuitIfSettingsNotSaved";
            this.WarnOnQuitIfSettingsNotSaved.Size = new Size(0xce, 0x11);
            this.WarnOnQuitIfSettingsNotSaved.TabIndex = 0x17;
            this.WarnOnQuitIfSettingsNotSaved.Text = "Warn on Quit if Settings are not saved";
            this.WarnOnQuitIfSettingsNotSaved.UseVisualStyleBackColor = true;
            this.WarnOnQuitIfSettingsNotSaved.Click += new EventHandler(this.SettingsChangeClick);
            this.RememberApplicationSizeAndPosition.AutoSize = true;
            this.RememberApplicationSizeAndPosition.Location = new Point(6, 0xf5);
            this.RememberApplicationSizeAndPosition.Name = "RememberApplicationSizeAndPosition";
            this.RememberApplicationSizeAndPosition.Size = new Size(0xd8, 0x11);
            this.RememberApplicationSizeAndPosition.TabIndex = 0x16;
            this.RememberApplicationSizeAndPosition.Text = "Remember Application Size and Position";
            this.RememberApplicationSizeAndPosition.UseVisualStyleBackColor = true;
            this.RememberApplicationSizeAndPosition.Click += new EventHandler(this.SettingsChangeClick);
            this.StartMinimized.AutoSize = true;
            this.StartMinimized.Location = new Point(6, 0xc7);
            this.StartMinimized.Name = "StartMinimized";
            this.StartMinimized.Size = new Size(0x61, 0x11);
            this.StartMinimized.TabIndex = 0x15;
            this.StartMinimized.Text = "Start Minimized";
            this.StartMinimized.UseVisualStyleBackColor = true;
            this.StartMinimized.Click += new EventHandler(this.SettingsChangeClick);
            this.SaveSettingsOnQuit.AutoSize = true;
            this.SaveSettingsOnQuit.Location = new Point(6, 0x10c);
            this.SaveSettingsOnQuit.Name = "SaveSettingsOnQuit";
            this.SaveSettingsOnQuit.Size = new Size(0xb6, 0x11);
            this.SaveSettingsOnQuit.TabIndex = 20;
            this.SaveSettingsOnQuit.Text = "Save Settings and Filters On Quit";
            this.SaveSettingsOnQuit.UseVisualStyleBackColor = true;
            this.SaveSettingsOnQuit.Click += new EventHandler(this.SettingsChangeClick);
            this.MinimizeToSystemTray.AutoSize = true;
            this.MinimizeToSystemTray.Location = new Point(6, 0xde);
            this.MinimizeToSystemTray.Name = "MinimizeToSystemTray";
            this.MinimizeToSystemTray.Size = new Size(0x8f, 0x11);
            this.MinimizeToSystemTray.TabIndex = 0x13;
            this.MinimizeToSystemTray.Text = "Minimize To System Tray";
            this.MinimizeToSystemTray.UseVisualStyleBackColor = true;
            this.MinimizeToSystemTray.Click += new EventHandler(this.SettingsChangeClick);
            this.BaseURL.DropDownStyle = ComboBoxStyle.DropDownList;
            this.BaseURL.FormattingEnabled = true;
            object[] objArray5 = new object[] { "www.torrentleech.org", "www.torrentleech.me", "www.torrentleech.cc" };
            this.BaseURL.Items.AddRange(objArray5);
            this.BaseURL.Location = new Point(0x72, 0x1aa);
            this.BaseURL.Name = "BaseURL";
            this.BaseURL.Size = new Size(0xad, 0x15);
            this.BaseURL.TabIndex = 0x12;
            this.BaseURL.Click += new EventHandler(this.SettingsChangeClick);
            this.label14.AutoSize = true;
            this.label14.Location = new Point(6, 0x1ad);
            this.label14.Name = "label14";
            this.label14.Size = new Size(0x38, 13);
            this.label14.TabIndex = 0x11;
            this.label14.Text = "Base URL";
            this.DoubleClickAction.DropDownStyle = ComboBoxStyle.DropDownList;
            this.DoubleClickAction.FormattingEnabled = true;
            object[] objArray6 = new object[] { "Test", "Open URL", "Save To Target", "Open in Torrent Client" };
            this.DoubleClickAction.Items.AddRange(objArray6);
            this.DoubleClickAction.Location = new Point(0x72, 0x18f);
            this.DoubleClickAction.Name = "DoubleClickAction";
            this.DoubleClickAction.Size = new Size(0x79, 0x15);
            this.DoubleClickAction.TabIndex = 0x10;
            this.DoubleClickAction.TextChanged += new EventHandler(this.DoubleClickAction_TextChanged);
            this.DoubleClickAction.Click += new EventHandler(this.SettingsChangeClick);
            this.label5.AutoSize = true;
            this.label5.Location = new Point(6, 0x192);
            this.label5.Name = "label5";
            this.label5.Size = new Size(100, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Double-Click Action";
            this.IgnoreInvalidTLSCertificates.AutoSize = true;
            this.IgnoreInvalidTLSCertificates.Location = new Point(6, 0x99);
            this.IgnoreInvalidTLSCertificates.Name = "IgnoreInvalidTLSCertificates";
            this.IgnoreInvalidTLSCertificates.Size = new Size(0xa8, 0x11);
            this.IgnoreInvalidTLSCertificates.TabIndex = 10;
            this.IgnoreInvalidTLSCertificates.Text = "Ignore Invalid TLS Certificates";
            this.IgnoreInvalidTLSCertificates.UseVisualStyleBackColor = true;
            this.IgnoreInvalidTLSCertificates.Click += new EventHandler(this.SettingsChangeClick);
            this.label4.AutoSize = true;
            this.label4.ForeColor = Color.Red;
            this.label4.Location = new Point(0xf1, 0x35);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0x9f, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Placeholder - Next major release";
            this.label15.AutoSize = true;
            this.label15.ForeColor = Color.Red;
            this.label15.Location = new Point(0xf1, 80);
            this.label15.Name = "label15";
            this.label15.Size = new Size(0x9f, 13);
            this.label15.TabIndex = 8;
            this.label15.Text = "Placeholder - Next major release";
            this.ThemeLabel.AutoSize = true;
            this.ThemeLabel.Location = new Point(3, 80);
            this.ThemeLabel.Name = "ThemeLabel";
            this.ThemeLabel.Size = new Size(40, 13);
            this.ThemeLabel.TabIndex = 7;
            this.ThemeLabel.Text = "Theme";
            this.Theme.BackColor = SystemColors.Window;
            this.Theme.DropDownStyle = ComboBoxStyle.DropDownList;
            this.Theme.Enabled = false;
            this.Theme.FormattingEnabled = true;
            this.Theme.Location = new Point(0x72, 0x4d);
            this.Theme.Name = "Theme";
            this.Theme.Size = new Size(0x79, 0x15);
            this.Theme.TabIndex = 6;
            this.Theme.Click += new EventHandler(this.SettingsChangeClick);
            this.UseRegexFilters.AutoSize = true;
            this.UseRegexFilters.Location = new Point(6, 130);
            this.UseRegexFilters.Name = "UseRegexFilters";
            this.UseRegexFilters.Size = new Size(0xe0, 0x11);
            this.UseRegexFilters.TabIndex = 5;
            this.UseRegexFilters.Text = "Use Regex Filters  (Advanced Users Only)";
            this.UseRegexFilters.UseVisualStyleBackColor = true;
            this.UseRegexFilters.CheckedChanged += new EventHandler(this.UseRegexFilters_CheckedChanged);
            this.UseRegexFilters.Click += new EventHandler(this.SettingsChangeClick);
            this.SettingsToolbar.ImageScalingSize = new Size(0x18, 0x18);
            ToolStripItem[] itemArray5 = new ToolStripItem[] { this.SaveSettingsButton1, this.toolStripSeparator3, this.RefreshLanguagesAndThemesButton };
            this.SettingsToolbar.Items.AddRange(itemArray5);
            this.SettingsToolbar.Location = new Point(3, 0x10);
            this.SettingsToolbar.Name = "SettingsToolbar";
            this.SettingsToolbar.Size = new Size(0x197, 0x1f);
            this.SettingsToolbar.TabIndex = 4;
            this.SettingsToolbar.Text = "toolStrip1";
            this.SaveSettingsButton1.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.SaveSettingsButton1.Image = (Image) manager.GetObject("SaveSettingsButton1.Image");
            this.SaveSettingsButton1.ImageTransparentColor = Color.Magenta;
            this.SaveSettingsButton1.Name = "SaveSettingsButton1";
            this.SaveSettingsButton1.Size = new Size(0x1c, 0x1c);
            this.SaveSettingsButton1.Text = "Save Settings";
            this.SaveSettingsButton1.Click += new EventHandler(this.SaveSettingsButton1_Click);
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new Size(6, 0x1f);
            this.RefreshLanguagesAndThemesButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.RefreshLanguagesAndThemesButton.Image = (Image) manager.GetObject("RefreshLanguagesAndThemesButton.Image");
            this.RefreshLanguagesAndThemesButton.ImageTransparentColor = Color.Magenta;
            this.RefreshLanguagesAndThemesButton.Name = "RefreshLanguagesAndThemesButton";
            this.RefreshLanguagesAndThemesButton.Size = new Size(0x1c, 0x1c);
            this.RefreshLanguagesAndThemesButton.Text = "Refresh Languages and Themes";
            this.DebugLevelText.AutoSize = true;
            this.DebugLevelText.Location = new Point(3, 0x6a);
            this.DebugLevelText.Name = "DebugLevelText";
            this.DebugLevelText.Size = new Size(0x44, 13);
            this.DebugLevelText.TabIndex = 3;
            this.DebugLevelText.Text = "Debug Level";
            this.DebugLevel.Location = new Point(0x72, 0x68);
            int[] numArray11 = new int[4];
            numArray11[0] = 9;
            this.DebugLevel.Maximum = new decimal(numArray11);
            this.DebugLevel.Name = "DebugLevel";
            this.DebugLevel.Size = new Size(0x79, 20);
            this.DebugLevel.TabIndex = 2;
            int[] numArray12 = new int[4];
            numArray12[0] = 7;
            this.DebugLevel.Value = new decimal(numArray12);
            this.DebugLevel.Click += new EventHandler(this.SettingsChangeClick);
            this.Language.DropDownStyle = ComboBoxStyle.DropDownList;
            this.Language.Enabled = false;
            this.Language.FormattingEnabled = true;
            object[] objArray7 = new object[] { "English", "Pirate", "Latin", "Pig Latin" };
            this.Language.Items.AddRange(objArray7);
            this.Language.Location = new Point(0x72, 50);
            this.Language.Name = "Language";
            this.Language.Size = new Size(0x79, 0x15);
            this.Language.TabIndex = 1;
            this.Language.Click += new EventHandler(this.SettingsChangeClick);
            this.LanguageLabel.AutoSize = true;
            this.LanguageLabel.Location = new Point(3, 0x35);
            this.LanguageLabel.Name = "LanguageLabel";
            this.LanguageLabel.Size = new Size(0x37, 13);
            this.LanguageLabel.TabIndex = 0;
            this.LanguageLabel.Text = "Language";
            this.DebugTab.Controls.Add(this.DebugLog);
            this.DebugTab.Controls.Add(this.groupBox5);
            this.DebugTab.ImageIndex = 8;
            this.DebugTab.Location = new Point(4, 0x23);
            this.DebugTab.Name = "DebugTab";
            this.DebugTab.Padding = new Padding(3);
            this.DebugTab.Size = new Size(0x525, 0x237);
            this.DebugTab.TabIndex = 3;
            this.DebugTab.Text = "Debug";
            this.DebugTab.ToolTipText = "Here you can see information and errors pertaining to the operation of this application";
            this.DebugTab.UseVisualStyleBackColor = true;
            this.DebugLog.Dock = DockStyle.Fill;
            this.DebugLog.Location = new Point(3, 100);
            this.DebugLog.Name = "DebugLog";
            this.DebugLog.Size = new Size(0x51f, 0x1d0);
            this.DebugLog.TabIndex = 2;
            this.DebugLog.Text = "";
            this.groupBox5.Controls.Add(this.richTextBox2);
            this.groupBox5.Dock = DockStyle.Top;
            this.groupBox5.Location = new Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new Size(0x51f, 0x61);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Important Information ";
            this.richTextBox2.BorderStyle = BorderStyle.None;
            this.richTextBox2.Dock = DockStyle.Fill;
            this.richTextBox2.Location = new Point(3, 0x10);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new Size(0x519, 0x4e);
            this.richTextBox2.TabIndex = 0;
            this.richTextBox2.Text = manager.GetString("richTextBox2.Text");
            this.notifyIcon1.Icon = (Icon) manager.GetObject("notifyIcon1.Icon");
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new EventHandler(this.notifyIcon1_DoubleClick);
            this.IdleTimer.Interval = 0x36ee80;
            this.IdleTimer.Tick += new EventHandler(this.timer1_Tick);
            this.toolTip1.AutoPopDelay = 0xea60;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 100;
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x52d, 0x2c9);
            base.Controls.Add(this.MainTabControl);
            base.Controls.Add(this.panel1);
            base.Controls.Add(this.statusStrip1);
            this.DoubleBuffered = true;
            base.Icon = (Icon) manager.GetObject("$this.Icon");
            base.Name = "TLTD";
            this.Text = "TLTD";
            base.FormClosing += new FormClosingEventHandler(this.TLTD_FormClosing);
            base.Load += new EventHandler(this.TLTD_Load);
            base.Shown += new EventHandler(this.TLTD_Shown);
            base.Resize += new EventHandler(this.TLTD_Resize);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.DefaultActionToolbar.ResumeLayout(false);
            this.DefaultActionToolbar.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.MainTabControl.ResumeLayout(false);
            this.AnnouncedTorrentsTab.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.FiltersTab.ResumeLayout(false);
            this.FiltersTab.PerformLayout();
            ((ISupportInitialize) this.Filters).EndInit();
            this.RegexToolbox.ResumeLayout(false);
            this.RegexToolbox.PerformLayout();
            this.FiltersToolbar.ResumeLayout(false);
            this.FiltersToolbar.PerformLayout();
            this.SettingsTab.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.IRCPort.EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.DeleteLogDays.EndInit();
            this.FreeRequirement.EndInit();
            this.SettingsToolbar.ResumeLayout(false);
            this.SettingsToolbar.PerformLayout();
            this.DebugLevel.EndInit();
            this.DebugTab.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void ircClient_Closed(object sender, EventArgs e)
        {
            this.LogDebug("Disconnected from IRC", 0);
            this.StatusLabel.Text = "OFFLINE";
            this.StatusLabel.ForeColor = Color.Red;
            this.GoOnlineButton.Enabled = true;
            this.IdleTimer.Enabled = false;
            if (!this.offlinewanted)
            {
                this.LogDebug("This disconnect was unwanted!", 1);
                this.retries++;
                if (this.retries > 3)
                {
                    this.LogDebug("We have reached our maximum number of retries.  Please reconnect manually", 1);
                }
                else
                {
                    this.GoOnlineButton.PerformClick();
                }
            }
            this.offlinewanted = false;
        }

        private void ircClient_Connected(object sender, EventArgs e)
        {
            this.LogDebug("Connected to IRC", 0);
            this.StatusLabel.Text = "ONLINE";
            this.StatusLabel.ForeColor = Color.Green;
            this.GoOfflineButton.Enabled = true;
            this.IdleTimer.Enabled = true;
        }

        private void ircClient_GotIrcError(object sender, IrcErrorEventArgs e)
        {
            int replyCode = (int) e.Data.ReplyCode;
            string[] textArray1 = new string[] { "GotIrcError : ", replyCode.ToString(), " (", e.Data.ReplyCode.ToString(), ")" };
            this.LogDebug(string.Concat(textArray1), 1);
            IrcReplyCode code = e.Data.ReplyCode;
            if (code == IrcReplyCode.NicknameInUse)
            {
                this.AltNick++;
                Settings.Instance.UserInfo.Nickname = this.BotNickname.Text + this.AltNick.ToString();
                this.LogDebug("Switching to nickname " + Settings.Instance.UserInfo.Nickname, 1);
                this.Connection.LogIn(Settings.Instance.Identd.UserID, Settings.Instance.UserInfo.RealName, Settings.Instance.UserInfo.Nickname, null, null, null);
            }
            else if (code == IrcReplyCode.BannedFromChannel)
            {
                this.GoOfflineButton.PerformClick();
                if (MessageBox.Show("Whoops!  Your bot has been banned from the #tlannounces channel!  Would you like to open up the web chat so you can speak with a support agent?", "Bot Banned!", MessageBoxButtons.YesNo, MessageBoxIcon.Hand) == DialogResult.Yes)
                {
                    Process.Start("https://" + this.BaseURL.Text + "/user/chat/guest");
                }
            }
        }

        private void ircClient_GotMessage(object sender, ChatMessageEventArgs e)
        {
            string text1 = e.Sender.ToString();
            string str = text1.Substring(0, text1.IndexOf("!"));
            string str2 = e.Recipient.ToString().ToLower();
            string release = e.Message.ToString();
            if (this.AnnounceChan.Text.ToLower().Contains(str2) && this.Announcers.Text.Contains(str))
            {
                this.AddRelease(release);
            }
            else
            {
                this.LogDebug("Unsolicited message received from: " + str + " Sent to: " + str2, 2);
                this.LogDebug("Message : " + release, 2);
                if (Debugger.IsAttached)
                {
                    this.AddRelease(release);
                }
            }
        }

        private void ircClient_GotWelcomeMessage(object sender, SimpleMessageEventArgs e)
        {
            this.LogDebug("Successfully connected to IRC server as nickname " + Settings.Instance.UserInfo.Nickname, 0);
            this.ircClient.Join("#tlannounces", null);
        }

        private int IsMatch(GroupCollection m, out string action, out string tempfile, out string targetpath)
        {
            int num;
            action = "Do Nothing";
            tempfile = "";
            targetpath = this.Target.Text;
            this.LogDebug("Testing release against filters", 5);
            using (IEnumerator enumerator = ((IEnumerable) this.Filters.Rows).GetEnumerator())
            {
                while (true)
                {
                    if (enumerator.MoveNext())
                    {
                        DataGridViewRow current = (DataGridViewRow) enumerator.Current;
                        if (current.Index >= this.Filters.Rows.Count)
                        {
                            continue;
                        }
                        this.LogDebug("Testing Release against row " + current.Index.ToString(), 6);
                        if (!bool.Parse(current.Cells[0].Value.ToString()))
                        {
                            continue;
                        }
                        if (string.IsNullOrEmpty(this.SafeRead(current.Cells[2].Value, "")) && string.IsNullOrEmpty(this.SafeRead(current.Cells[3].Value, "")))
                        {
                            if (!this.TryMatch(m[2].ToString(), this.SafeRead(current.Cells[4].Value, ""), "Negate Title", true, false) || !this.TryMatch(m[1].ToString(), this.SafeRead(current.Cells[5].Value, ""), "Negate Category", true, false))
                            {
                                continue;
                            }
                            this.LogDebug("This Negate Row matched!", 6);
                            num = 2;
                        }
                        else
                        {
                            if (!this.TryMatch(m[4].ToString(), this.SafeRead(current.Cells[1].Value, ""), "Freeleech", false, true) || (!this.TryMatch(m[2].ToString(), this.SafeRead(current.Cells[2].Value, ""), "Title", true, false) || (!this.TryMatch(m[1].ToString(), this.SafeRead(current.Cells[3].Value, ""), "Category", true, false) || (this.TryMatch(m[2].ToString(), this.SafeRead(current.Cells[4].Value, ""), "Negate Title", false, false) || this.TryMatch(m[1].ToString(), this.SafeRead(current.Cells[5].Value, ""), "Negate Category", false, false)))))
                            {
                                continue;
                            }
                            if ((this.SafeRead(current.Cells[6].Value, "") != "") || (this.SafeRead(current.Cells[7].Value, "") != ""))
                            {
                                tempfile = this.DownloadTorrentToTemp(m[2].ToString(), m[5].ToString(), m[6].ToString(), this.RSSKey.Text);
                                if (tempfile == "fail")
                                {
                                    this.LogDebug("We were unsuccessful in downloading this file to a temp file, so therefore we cannot test the size requirement.  Therefore, this filter did not match.", 5);
                                    num = 0;
                                    break;
                                }
                                if (!this.OKSize(this.TorrentSize(tempfile), this.ZeroIfEmpty(this.SafeRead(current.Cells[6].Value, "")), this.ZeroIfEmpty(this.SafeRead(current.Cells[7].Value, ""))))
                                {
                                    this.LogDebug("Size is NOT within your specified requirements", 5);
                                    num = 0;
                                    break;
                                }
                                this.LogDebug("Size is within your specified requirements", 5);
                            }
                            this.LogDebug("This Row matched! ", 6);
                            try
                            {
                                action = this.SafeRead(current.Cells[8].Value, "");
                                if (this.SafeRead(current.Cells[9].Value, "") != "")
                                {
                                    targetpath = this.SafeRead(current.Cells[9].Value, "");
                                }
                            }
                            catch
                            {
                            }
                            num = 1;
                        }
                    }
                    else
                    {
                        this.LogDebug("Release did not match any filters", 5);
                        return 0;
                    }
                    break;
                }
            }
            return num;
        }

        private void LogDebug(string msg, int level)
        {
            if (level <= this.DebugLevel.Value)
            {
                msg = DateTime.Now.ToString() + " : " + msg + Environment.NewLine;
                this.DebugLog.AppendText(msg);
                if (this.SaveDebugLog.Checked)
                {
                    System.IO.File.AppendAllText(this.debugfilename, msg);
                }
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            base.Show();
            base.WindowState = FormWindowState.Normal;
        }

        private long Numerize(string val)
        {
            try
            {
                return long.Parse(val.Replace("M", "000000").Replace("G", "000000000").Replace("T", "000000000000").Replace(" ", ""));
            }
            catch
            {
                return 0L;
            }
        }

        private bool OKSize(long size, string min, string max)
        {
            this.LogDebug("Checking If the Torrent Size is OK as per the minimim size " + min + " and maximum size " + max, 6);
            long num = this.Numerize(min);
            long num2 = this.Numerize(max);
            num2 ??= 0x7fffffffffffffffL;
            this.LogDebug("Translated minsize to be " + num.ToString(), 6);
            this.LogDebug("Translated maxsize to be " + num2.ToString(), 6);
            if ((size < num) || (size > num2))
            {
                return false;
            }
            this.LogDebug($"Size of torrent is {size} which is greater or equal to {num} and less than or equal to {num2}", 6);
            return true;
        }

        private void OnFTPLogEvent(FtpTraceLevel ftpTraceLevel, string logMessage)
        {
            switch (ftpTraceLevel)
            {
                case FtpTraceLevel.Verbose:
                    this.LogDebug(logMessage, 7);
                    return;

                case FtpTraceLevel.Warn:
                    this.LogDebug(logMessage, 7);
                    return;

                case FtpTraceLevel.Error:
                    this.LogDebug(logMessage, 7);
                    return;
            }
            this.LogDebug(logMessage, 7);
        }

        private void PerformAction(string actiontype, string title, string torrentid, string targetpath, string targeturl)
        {
            this.LogDebug("Performing menu action " + actiontype + " on Torrent title " + title, 0);
            string path = "";
            if (actiontype == "Do Nothing")
            {
                this.LogDebug("Well.  My action is to do nothing.  So here I am, doing nothing with this match.  Oh well.  (Twiddles thumbs)", 0);
            }
            else if (actiontype == "Test")
            {
                path = this.DownloadTorrentToTemp(title, this.BaseURL.Text + "/", torrentid, this.RSSKey.Text);
                if (path == "fail")
                {
                    this.LogDebug("My Action was set to Test.  I tested, and failed :(  Make sure your RSS Key is set and that it is correct.  Should be the last 20 characters of your RSS Feed URL", 0);
                }
                else if (System.IO.File.Exists(path))
                {
                    this.LogDebug("The test was successful!  File " + path + " was saved", 0);
                }
                else
                {
                    this.LogDebug("Not sure what happened, but apparently, our test failed.  Please check debug log for further information", 0);
                }
            }
            else if (actiontype == "Notify")
            {
                string str2 = "Name: " + title + " - download URL: " + targeturl;
                this.LogDebug("Creating Notify for " + str2, 0);
                this.notifyIcon1.BalloonTipTitle = "Torrent Match Detected";
                this.notifyIcon1.BalloonTipText = str2;
                this.notifyIcon1.ShowBalloonTip(0x1388);
            }
            else if (actiontype == "Open URL")
            {
                this.LogDebug("Launching URL " + targeturl, 0);
                Process.Start(targeturl);
            }
            else if (actiontype == "Save To Target")
            {
                path = this.DownloadTorrentToTemp(title, this.BaseURL.Text + "/", torrentid, this.RSSKey.Text);
                if (path == "fail")
                {
                    this.LogDebug("My Action was set to Download.  The download of the file failed  :(  Make sure your RSS Key is set and that it is correct.  Should be the last 20 characters of your RSS Feed URL", 0);
                }
                else if (System.IO.File.Exists(path))
                {
                    this.SaveTotarget(path, targetpath, this.SafeFileName(title));
                }
                else
                {
                    this.LogDebug("We were unable to download the release to a temporary file.  Please check debug log for further information", 0);
                }
            }
            else if (actiontype != "Open in Torrent Client")
            {
                this.LogDebug("The selected action has not yet been implemented.  Please contact your favourite developer for assistance.", 0);
            }
            else
            {
                path = this.DownloadTorrentToTemp(title, this.BaseURL.Text + "/", torrentid, this.RSSKey.Text);
                if (path == "fail")
                {
                    this.LogDebug("My Action was set to Add to Torrent Client.  The download of the file failed  :(  Make sure your RSS Key is set and that it is correct.  Should be the last 20 characters of your RSS Feed URL", 0);
                }
                else if (!System.IO.File.Exists(path) || !this.EnoughDiskSpace(targetpath, this.TorrentSize(path)))
                {
                    this.LogDebug("We were unable to download the release to a temporary file.  Please check debug log for further information", 0);
                }
                else
                {
                    string str3 = this.commonpath + @"\TLTD\Downloads\";
                    string destFileName = str3 + this.SafeFileName(title) + ".torrent";
                    if (!Directory.Exists(str3))
                    {
                        Directory.CreateDirectory(str3);
                    }
                    this.LogDebug("Copying from temp folder to " + str3, 0);
                    try
                    {
                        System.IO.File.Move(path, destFileName);
                        Process.Start(destFileName);
                    }
                    catch (Exception exception)
                    {
                        this.LogDebug("Failed to move file.  Exception message: " + exception.Message, 0);
                    }
                }
            }
            if (System.IO.File.Exists(path))
            {
                this.LogDebug("Deleting Temporary file " + path, 8);
                System.IO.File.Delete(path);
            }
        }

        private void PerformActions(string actiontype)
        {
            foreach (ListViewItem item1 in this.TorrentList.SelectedItems)
            {
                string text = this.Target.Text;
                string title = item1.SubItems[2].Text;
                string torrentid = item1.SubItems[5].Text;
                string targeturl = "https://" + this.BaseURL.Text + "/torrent/" + torrentid;
                this.PerformAction(actiontype, title, torrentid, text, targeturl);
            }
        }

        private void Readoptions()
        {
            string filename = this.commonpath + @"\TLTD\Settings.xml";
            try
            {
                XmlDocument document = new XmlDocument();
                document.Load(filename);
                this.SiteUsername.Text = this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Control/SiteUsername"), "");
                this.RSSKey.Text = this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Control/RSSKey"), "");
                this.DefaultAction.Text = this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Control/DefaultAction"), "Open URL");
                this.Target.Text = this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Control/Target"), KnownFolders.GetPath(KnownFolder.Downloads));
                this.Language.Text = this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/Language"), "English");
                this.Theme.Text = this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/Theme"), "Light");
                this.DebugLevel.Value = int.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/DebugLevel"), "1"));
                this.UseRegexFilters.Checked = bool.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/UseRegexFilters"), "False"));
                this.MinimizeToSystemTray.Checked = bool.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/MinimizeToSystemTray"), "False"));
                this.SaveSettingsOnQuit.Checked = bool.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/SaveSettingsOnQuit"), "False"));
                this.GoOnlineAtStartup.Checked = bool.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/GoOnlineAtStartup"), "False"));
                this.RememberApplicationSizeAndPosition.Checked = bool.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/RememberApplicationSizeAndPosition"), "False"));
                this.StartMinimized.Checked = bool.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/StartMinimized"), "False"));
                this.WarnOnQuitIfSettingsNotSaved.Checked = bool.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/WarnOnQuitIfSettingsNotSaved"), "True"));
                this.RequireAtLeast.Checked = bool.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/RequireAtLeast"), "False"));
                this.FreeRequirement.Value = int.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/FreeRequirement"), "50"));
                this.FreeUnitOfMeasure.Text = this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/FreeUnitOfMeasure"), "Gigabytes (GB)");
                this.SaveDebugLog.Checked = bool.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/SaveDebugLog"), "False"));
                this.DeleteLogsOlderThan.Checked = bool.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/DeleteLogsOlderThan"), "False"));
                this.DeleteLogDays.Value = int.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/DeleteLogDays"), "30"));
                if (this.RememberApplicationSizeAndPosition.Checked)
                {
                    try
                    {
                        string str2 = this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/WindowPosition"), "");
                        if (str2 != null)
                        {
                            char[] separator = new char[] { ',' };
                            Func<string, int> selector = <>c.<>9__38_0;
                            if (<>c.<>9__38_0 == null)
                            {
                                Func<string, int> local1 = <>c.<>9__38_0;
                                selector = <>c.<>9__38_0 = v => int.Parse(v);
                            }
                            List<int> list2 = str2.Split(separator, StringSplitOptions.RemoveEmptyEntries).Select<string, int>(selector).ToList<int>();
                            if (list2.Count == 5)
                            {
                                base.SetBounds(list2[1], list2[2], list2[3], list2[4]);
                                base.WindowState = list2[0];
                            }
                        }
                    }
                    catch
                    {
                    }
                }
                if (this.StartMinimized.Checked)
                {
                    base.WindowState = FormWindowState.Minimized;
                }
                this.IgnoreInvalidTLSCertificates.Checked = bool.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/IgnoreInvalidTLSCertificates"), "False"));
                this.FilterHelp.Visible = bool.Parse(this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/FilterHelpVisible"), "False"));
                this.DoubleClickAction.Text = this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/DoubleClickAction"), "Open URL");
                this.BaseURL.Text = this.SafeXMLNodeRead(document.DocumentElement.SelectSingleNode("/root/Preferences/BaseURL"), "www.torrentleech.org");
                XmlNodeList list = document.DocumentElement.SelectNodes("/root/Filters/Filter");
                if (list != null)
                {
                    this.LogDebug("Number of Filters discovered: " + list.Count.ToString(), 0);
                    this.Filters.Rows.Clear();
                    foreach (XmlNode node in list)
                    {
                        object[] values = new object[10];
                        values[0] = bool.Parse(this.SafeXMLNodeRead(node.SelectSingleNode("Enabled"), "false"));
                        values[1] = this.SafeXMLNodeRead(node.SelectSingleNode("Freeleech"), "Don't Care");
                        values[2] = this.SafeXMLNodeRead(node.SelectSingleNode("Title"), "");
                        values[3] = this.SafeXMLNodeRead(node.SelectSingleNode("Category"), "");
                        values[4] = this.SafeXMLNodeRead(node.SelectSingleNode("ExcludeTitle"), "");
                        values[5] = this.SafeXMLNodeRead(node.SelectSingleNode("ExcludeCategory"), "");
                        values[6] = this.SafeXMLNodeRead(node.SelectSingleNode("MinSize"), "");
                        values[7] = this.SafeXMLNodeRead(node.SelectSingleNode("MaxSize"), "");
                        values[8] = this.SafeXMLNodeRead(node.SelectSingleNode("Action"), "Use Default");
                        values[9] = this.SafeXMLNodeRead(node.SelectSingleNode("IndividualTarget"), "");
                        this.Filters.Rows.Add(values);
                    }
                }
                this.LogDebug("Successfully loaded all preferences.", 0);
            }
            catch (Exception exception)
            {
                this.LogDebug("Error loading preferences.  Will use default.  This is normal if this is the first time you are running this application.", 0);
                this.LogDebug("The Error Was: " + exception.Message, 0);
                Wizard wizard1 = new Wizard();
                wizard1.DataAvailable += new EventHandler(this.wizard_DataAvailable);
                wizard1.ShowDialog();
                if (this.SiteUsername.TextLength > 0)
                {
                    this.GoOnlineButton.PerformClick();
                }
            }
        }

        private void RegularExpression_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.MatchOrNoMatch.Text = !Regex.IsMatch(this.TestString.Text, this.RegularExpression.Text) ? "DOES NOT MATCH" : "MATCH!  WOO!!";
            }
            catch (Exception exception)
            {
                this.MatchOrNoMatch.Text = "Exception: " + exception.Message;
            }
        }

        private void ResetTrueName()
        {
            this.trueNickname = Settings.Instance.UserInfo.Nickname;
        }

        private void RSSKey_TextChanged(object sender, EventArgs e)
        {
            Match match = new Regex(@"rss.+org\/([a-f,0-9]{20})").Match(this.RSSKey.Text);
            if (match.Success)
            {
                this.RSSKey.Text = match.Groups[1].Value;
            }
        }

        private string SafeFileName(string filename) => 
            filename.Replace("<", "_").Replace(">", "_").Replace(":", "_").Replace('"', '_').Replace(":", "_").Replace("/", "_").Replace(@"\", "_").Replace("|", "_").Replace("?", "_").Replace("*", "_").Replace(" ", ".");

        private string SafeRead(object value, string defaultval = "") => 
            (value == null) ? defaultval : value.ToString();

        private string SafeXMLNodeRead(XmlNode value, string defaultval = "") => 
            (value == null) ? defaultval : value.InnerText.ToString();

        private void SaveSettingsButton1_Click(object sender, EventArgs e)
        {
            this.WriteOptions();
        }

        private void SaveSettingsButton2_Click(object sender, EventArgs e)
        {
            this.WriteOptions();
        }

        private void SaveTotarget(string tempfile, string targetpath, string title)
        {
            char[] separator = new char[] { ',' };
            foreach (string str in targetpath.Split(separator))
            {
                Match match = new Regex(@"^(scp|s?ftp|ftps?):\/\/(.+):(.+)@(.+?)(\/.*)$").Match(targetpath);
                if (!match.Success)
                {
                    if (!Directory.Exists(str))
                    {
                        this.LogDebug("Invalid Path " + str + " Specified.  A supported path is a local folder or network share that exists. Network shares can be in the format of UNC Path or URL, such as ftp://, ftps:// and sftp://", 0);
                    }
                    else if (this.EnoughDiskSpace(str, this.TorrentSize(tempfile)))
                    {
                        string path = Path.Combine(str, title.Replace(" ", ".") + ".torrent");
                        if (!System.IO.File.Exists(path))
                        {
                            this.LogDebug("Copying from temp folder to " + path, 0);
                            try
                            {
                                System.IO.File.Copy(tempfile, path);
                            }
                            catch (Exception exception)
                            {
                                this.LogDebug("Failed to copy file.  Exception message: " + exception.Message, 0);
                            }
                        }
                    }
                }
                else if (this.FTPUpload(match.Groups[1].Value, match.Groups[2].Value, match.Groups[3].Value, match.Groups[4].Value, match.Groups[5].Value, tempfile, title))
                {
                    string[] textArray1 = new string[] { "Successfully sent release to ", match.Groups[1].Value, "://", match.Groups[4].Value, "/", match.Groups[5].Value };
                    this.LogDebug(string.Concat(textArray1), 0);
                }
                else
                {
                    string[] textArray2 = new string[] { "Failed to send release to ", match.Groups[1].Value, "://", match.Groups[4].Value, "/", match.Groups[5].Value };
                    this.LogDebug(string.Concat(textArray2), 0);
                }
            }
        }

        private void SelectedTheme_TextChanged(object sender, EventArgs e)
        {
            TLTD.ColorScheme.Theme selectedIndex = (TLTD.ColorScheme.Theme) this.Theme.SelectedIndex;
            this.ChangeTheme(new TLTD.ColorScheme(selectedIndex), Form.ActiveForm.Controls);
            this.ChangeTabTheme(new TLTD.ColorScheme(selectedIndex), this.MainTabControl);
        }

        private void SettingsChangeClick(object sender, EventArgs e)
        {
            this.settingschanged = true;
        }

        private void SetupDebugLogging()
        {
            string str = DateTime.Now.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            this.debugfilename = this.commonpath + @"\TLTD\Logs\Debug-" + str + ".log";
            new FileInfo(this.debugfilename).Directory.Create();
        }

        private void SiteUsername_TextChanged(object sender, EventArgs e)
        {
            if (this.SiteUsername.TextLength <= 0)
            {
                this.BotNickname.Text = this.SiteUsername.Text;
            }
            else if (Regex.IsMatch(this.SiteUsername.Text, @"^\d+"))
            {
                this.BotNickname.Text = "tluser" + this.SiteUsername.Text + "_bot";
            }
            else
            {
                this.BotNickname.Text = this.SiteUsername.Text + "_bot";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.LogDebug("I haven't heard any announces for the last hour.  Forcing abnormal disconnect from IRC.", 0);
            this.Disconnect();
        }

        private void TLTD_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.SaveSettingsOnQuit.Checked)
            {
                this.WriteOptions();
            }
            if (this.settingschanged && this.WarnOnQuitIfSettingsNotSaved.Checked)
            {
                DialogResult result1 = MessageBox.Show("You have not saved your settings!  Would you like to save your settings before exiting?", "Settings not Saved!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
                if (result1 == DialogResult.Yes)
                {
                    this.WriteOptions();
                }
                if (result1 == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
            this.DeleteOldLogs();
        }

        private void TLTD_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " v" + Assembly.GetExecutingAssembly().GetName().Version.ToString();
            this.Language.SelectedIndex = 0;
            this.FreeUnitOfMeasure.SelectedIndex = 0;
            this.DoubleClickAction.SelectedIndex = 1;
            this.BaseURL.SelectedIndex = 0;
            this.DefaultAction.SelectedIndex = 0;
            this.Theme.Items.AddRange(System.Enum.GetNames(typeof(TLTD.ColorScheme.Theme)));
            this.Theme.SelectedIndex = 0;
            this.Theme.TextChanged += new EventHandler(this.SelectedTheme_TextChanged);
        }

        private void TLTD_Resize(object sender, EventArgs e)
        {
            if ((base.WindowState == FormWindowState.Minimized) && this.MinimizeToSystemTray.Checked)
            {
                base.Hide();
            }
            else if (this.RememberApplicationSizeAndPosition.Checked)
            {
                this.settingschanged = true;
            }
        }

        private void TLTD_Shown(object sender, EventArgs e)
        {
            this.SetupDebugLogging();
            this.Readoptions();
            this.DeleteOldLogs();
            if (this.GoOnlineAtStartup.Checked && this.GoOnlineButton.Enabled)
            {
                this.GoOnlineButton.PerformClick();
            }
        }

        private void ToggleFilterHelpButton_Click(object sender, EventArgs e)
        {
            this.FilterHelp.Visible = !this.FilterHelp.Visible;
        }

        private void TorrentList_DoubleClick_1(object sender, EventArgs e)
        {
            this.PerformActions(this.DoubleClickAction.Text);
        }

        private long TorrentSize(string tempfile)
        {
            long num = 0L;
            try
            {
                StreamReader reader1 = new StreamReader(tempfile);
                string input = reader1.ReadLine();
                reader1.Close();
                input = Regex.Match(input, "(.+pieces[1-9][0-9]*:)").ToString();
                this.LogDebug("Bencoded Data: " + input, 6);
                foreach (Match match in new Regex(@"lengthi(\d+)").Matches(input))
                {
                    num += long.Parse(match.Groups[1].Value);
                }
            }
            catch (Exception exception)
            {
                this.LogDebug("Failed to read torrent file, could not determine size.  Exception is " + exception.Message, 6);
            }
            return num;
        }

        private bool TryMatch(string s, string f, string matchtype, bool emptyistrue = true, bool fl = false)
        {
            if (fl)
            {
                string[] textArray1 = new string[] { "Attempting ", matchtype, " Match '", s, "' with filter ", f };
                this.LogDebug(string.Concat(textArray1), 6);
                return ((f != "Don't Care") ? (((s != "freeleech") || (f != "Yes")) ? ((s == "") && (f == "No")) : true) : true);
            }
            char[] separator = new char[] { ',' };
            foreach (string str in f.Split(separator))
            {
                if (str == "")
                {
                    return emptyistrue;
                }
                if (this.UseRegexFilters.Checked)
                {
                    string[] textArray2 = new string[] { "Attempting ", matchtype, " Regex Match '", s, "' with filter ", str };
                    this.LogDebug(string.Concat(textArray2), 6);
                    if (Regex.IsMatch(s, str))
                    {
                        return true;
                    }
                }
                else
                {
                    string[] textArray3 = new string[] { "Attempting ", matchtype, " Match '", s, "' with filter ", str };
                    this.LogDebug(string.Concat(textArray3), 6);
                    if (s == str)
                    {
                        return true;
                    }
                    if (Regex.IsMatch(s, WildCardToRegular(str)))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void UseRegexFilters_CheckedChanged(object sender, EventArgs e)
        {
            this.RegexToolbox.Visible = this.UseRegexFilters.Checked;
        }

        private static string WildCardToRegular(string value) => 
            "^" + Regex.Escape(value).Replace(@"\?", ".").Replace(@"\*", ".*") + "$";

        private void wizard_DataAvailable(object sender, EventArgs e)
        {
            Wizard wizard = sender as Wizard;
            if (wizard != null)
            {
                this.SiteUsername.Text = wizard.username;
                this.RSSKey.Text = wizard.rsskey;
            }
        }

        private void WriteOptions()
        {
            XmlDocument doc;
            XmlElement getelement(string optionname, string value)
            {
                XmlElement element1 = doc.CreateElement(string.Empty, optionname, string.Empty);
                element1.InnerText = value;
                return element1;
            }
            string fileName = this.commonpath + @"\TLTD\Settings.xml";
            XmlWriterSettings settings1 = new XmlWriterSettings();
            settings1.Indent = true;
            settings1.IndentChars = "     ";
            settings1.CloseOutput = true;
            settings1.OmitXmlDeclaration = true;
            doc = new XmlDocument();
            XmlDeclaration newChild = doc.CreateXmlDeclaration("1.0", "UTF-8", string.Empty);
            doc.InsertBefore(newChild, doc.DocumentElement);
            XmlElement element2 = doc.CreateElement(string.Empty, "root", string.Empty);
            doc.AppendChild(element2);
            XmlElement element3 = doc.CreateElement(string.Empty, "Control", string.Empty);
            element2.AppendChild(element3);
            element3.AppendChild(getelement("SiteUsername", this.SiteUsername.Text));
            element3.AppendChild(getelement("RSSKey", this.RSSKey.Text));
            element3.AppendChild(getelement("DefaultAction", this.DefaultAction.Text));
            element3.AppendChild(getelement("Target", this.Target.Text));
            XmlElement element4 = doc.CreateElement(string.Empty, "Preferences", string.Empty);
            element2.AppendChild(element4);
            element4.AppendChild(getelement("Language", this.Language.Text));
            element4.AppendChild(getelement("SelectedTheme", this.Theme.Text));
            element4.AppendChild(getelement("DebugLevel", this.DebugLevel.Value.ToString()));
            element4.AppendChild(getelement("FilterHelpVisible", this.FilterHelp.Visible.ToString()));
            element4.AppendChild(getelement("UseRegexFilters", this.UseRegexFilters.Checked.ToString()));
            element4.AppendChild(getelement("MinimizeToSystemTray", this.MinimizeToSystemTray.Checked.ToString()));
            element4.AppendChild(getelement("RememberApplicationSizeAndPosition", this.RememberApplicationSizeAndPosition.Checked.ToString()));
            element4.AppendChild(getelement("StartMinimized", this.StartMinimized.Checked.ToString()));
            element4.AppendChild(getelement("GoOnlineAtStartup", this.GoOnlineAtStartup.Checked.ToString()));
            element4.AppendChild(getelement("SaveSettingsOnQuit", this.SaveSettingsOnQuit.Checked.ToString()));
            element4.AppendChild(getelement("WarnOnQuitIfSettingsNotSaved", this.WarnOnQuitIfSettingsNotSaved.Checked.ToString()));
            element4.AppendChild(getelement("IgnoreInvalidTLSCertificates", this.IgnoreInvalidTLSCertificates.Checked.ToString()));
            element4.AppendChild(getelement("DoubleClickAction", this.DoubleClickAction.Text));
            element4.AppendChild(getelement("BaseURL", this.BaseURL.Text));
            element4.AppendChild(getelement("RequireAtLeast", this.RequireAtLeast.Checked.ToString()));
            element4.AppendChild(getelement("FreeRequirement", this.FreeRequirement.Value.ToString()));
            element4.AppendChild(getelement("FreeUnitOfMeasure", this.FreeUnitOfMeasure.Text));
            element4.AppendChild(getelement("SaveDebugLog", this.SaveDebugLog.Checked.ToString()));
            element4.AppendChild(getelement("DeleteLogsOlderThan", this.DeleteLogsOlderThan.Checked.ToString()));
            element4.AppendChild(getelement("DeleteLogDays", this.DeleteLogDays.Value.ToString()));
            Rectangle rectangle = (base.WindowState == FormWindowState.Normal) ? base.DesktopBounds : base.RestoreBounds;
            element4.AppendChild(getelement("WindowPosition", $"{(int) base.WindowState},{rectangle.Left},{rectangle.Top},{rectangle.Width},{rectangle.Height}"));
            XmlElement element5 = doc.CreateElement(string.Empty, "Filters", string.Empty);
            element2.AppendChild(element5);
            foreach (DataGridViewRow row in (IEnumerable) this.Filters.Rows)
            {
                if (row.Index < this.Filters.Rows.Count)
                {
                    XmlElement element6 = doc.CreateElement(string.Empty, "Filter", string.Empty);
                    element5.AppendChild(element6);
                    element6.AppendChild(getelement("Enabled", this.SafeRead(row.Cells[0].Value, "")));
                    element6.AppendChild(getelement("Freeleech", this.SafeRead(row.Cells[1].Value, "")));
                    element6.AppendChild(getelement("Title", this.SafeRead(row.Cells[2].Value, "")));
                    element6.AppendChild(getelement("Category", this.SafeRead(row.Cells[3].Value, "")));
                    element6.AppendChild(getelement("ExcludeTitle", this.SafeRead(row.Cells[4].Value, "")));
                    element6.AppendChild(getelement("ExcludeCategory", this.SafeRead(row.Cells[5].Value, "")));
                    element6.AppendChild(getelement("MinSize", this.SafeRead(row.Cells[6].Value, "")));
                    element6.AppendChild(getelement("MaxSize", this.SafeRead(row.Cells[7].Value, "")));
                    element6.AppendChild(getelement("Action", this.SafeRead(row.Cells[8].Value, "")));
                    element6.AppendChild(getelement("IndividualTarget", this.SafeRead(row.Cells[9].Value, "")));
                }
            }
            new FileInfo(fileName).Directory.Create();
            doc.Save(fileName);
            this.settingschanged = false;
        }

        private string ZeroIfEmpty(string s) => 
            string.IsNullOrEmpty(s) ? "0" : s;

        internal IrcClient Connection =>
            this.ircClient;

        [Serializable, CompilerGenerated]
        private sealed class <>c
        {
            public static readonly TLTD.TLTD.<>c <>9 = new TLTD.TLTD.<>c();
            public static Func<string, int> <>9__38_0;

            internal int <Readoptions>b__38_0(string v) => 
                int.Parse(v);
        }
    }
}

