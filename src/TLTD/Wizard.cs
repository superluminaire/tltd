﻿namespace TLTD
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Windows.Forms;

    public class Wizard : Form
    {
        private IContainer components;
        private TextBox Username;
        private Label label1;
        private TextBox RSSKey;
        private Label label3;
        private Button button1;
        private Button button2;
        private LinkLabel linkLabel1;
        private ToolTip toolTip1;
        private Button button3;

        public event EventHandler DataAvailable;

        public Wizard()
        {
            this.InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.OnDataAvailable(null);
            base.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (File.Exists("TLTD User Guide.pdf"))
            {
                Process.Start("TLTD User Guide.pdf");
            }
            else
            {
                Process.Start("https://forums.torrentleech.org/t/tltd-user-guide/80036");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            this.Username = new TextBox();
            this.label1 = new Label();
            this.RSSKey = new TextBox();
            this.label3 = new Label();
            this.button1 = new Button();
            this.button2 = new Button();
            this.linkLabel1 = new LinkLabel();
            this.toolTip1 = new ToolTip(this.components);
            this.button3 = new Button();
            base.SuspendLayout();
            this.Username.Location = new Point(0x5d, 0x4b);
            this.Username.Name = "Username";
            this.Username.Size = new Size(0xd1, 20);
            this.Username.TabIndex = 0;
            this.toolTip1.SetToolTip(this.Username, "Enter your TL Username here.  This is NOT your e-mail address.");
            this.Username.KeyPress += new KeyPressEventHandler(this.Username_KeyPress);
            this.label1.AutoSize = true;
            this.label1.Location = new Point(0x10, 0x4e);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x47, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "TL Username";
            this.RSSKey.Location = new Point(0x5d, 100);
            this.RSSKey.Name = "RSSKey";
            this.RSSKey.Size = new Size(0xd1, 20);
            this.RSSKey.TabIndex = 3;
            this.toolTip1.SetToolTip(this.RSSKey, "Enter in your RSS Key here, if you have it.  We need this to download stuff for you!");
            this.RSSKey.UseSystemPasswordChar = true;
            this.RSSKey.KeyPress += new KeyPressEventHandler(this.RSSKey_KeyPress);
            this.label3.Dock = DockStyle.Top;
            this.label3.Location = new Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Padding = new Padding(5);
            this.label3.Size = new Size(0x171, 0x48);
            this.label3.TabIndex = 4;
            this.label3.Text = "It looks like this is the first time you are running this software!  For it to function you need at least your username and your RSS Key.  You can enter these now, or skip this wizard";
            this.label3.TextAlign = ContentAlignment.MiddleCenter;
            this.button1.DialogResult = DialogResult.Cancel;
            this.button1.Location = new Point(0x13, 0x8a);
            this.button1.Name = "button1";
            this.button1.Size = new Size(0x6c, 0x2a);
            this.button1.TabIndex = 5;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new EventHandler(this.button1_Click);
            this.button2.Location = new Point(0xf7, 0x8a);
            this.button2.Name = "button2";
            this.button2.Size = new Size(0x6c, 0x2a);
            this.button2.TabIndex = 6;
            this.button2.Text = "Open User Guide";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new EventHandler(this.button2_Click);
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new Point(0x10, 0x67);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new Size(50, 13);
            this.linkLabel1.TabIndex = 7;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "RSS Key";
            this.toolTip1.SetToolTip(this.linkLabel1, "Click me if you want me to take you to the place where you would enable RSS and find your RSS URL's.  Paste the URL in the box to the right, and we'll extract the RSS key from there.");
            this.linkLabel1.LinkClicked += new LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            this.toolTip1.AutoPopDelay = 0xea60;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 100;
            this.toolTip1.ToolTipIcon = ToolTipIcon.Info;
            this.button3.DialogResult = DialogResult.Cancel;
            this.button3.Location = new Point(0x85, 0x89);
            this.button3.Name = "button3";
            this.button3.Size = new Size(0x6c, 0x2a);
            this.button3.TabIndex = 8;
            this.button3.Text = "Skip";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new EventHandler(this.button3_Click);
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this.button1;
            base.ClientSize = new Size(0x171, 0xbf);
            base.Controls.Add(this.button3);
            base.Controls.Add(this.linkLabel1);
            base.Controls.Add(this.button2);
            base.Controls.Add(this.button1);
            base.Controls.Add(this.label3);
            base.Controls.Add(this.RSSKey);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.Username);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "Wizard";
            base.ShowIcon = false;
            base.StartPosition = FormStartPosition.CenterParent;
            this.Text = "First Start Wizard";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.Username.TextLength > 0)
            {
                Process.Start("https://www.torrentleech.org/profile/" + this.Username.Text + "/edit");
            }
            else
            {
                MessageBox.Show("If you'd be so kind as to type in your username, I'll take you to the place where you will find the option to enable RSS (if disabled), and it will show you your RSS links.  You can use either one of them!", "Need to Know your Name First!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        protected virtual void OnDataAvailable(EventArgs e)
        {
            if (this.DataAvailable == null)
            {
                EventHandler dataAvailable = this.DataAvailable;
            }
            else
            {
                this.DataAvailable(this, e);
            }
        }

        private void RSSKey_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                this.OnDataAvailable(null);
                base.Close();
            }
        }

        private void Username_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                base.ActiveControl = this.RSSKey;
            }
        }

        public string username
        {
            get => 
                this.Username.Text;
            set => 
                this.Username.Text = value;
        }

        public string rsskey
        {
            get => 
                this.RSSKey.Text;
            set => 
                this.RSSKey.Text = value;
        }
    }
}

