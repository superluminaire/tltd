﻿namespace TLTD
{
    using System;
    using System.Drawing;

    internal class ColorScheme
    {
        private Color formbg;
        private Color formfg;
        private Color panelbg;
        private Color panelfg;
        private Color buttonbg;
        private Color buttonfg;
        private Color textboxbg;
        private Color textboxfg;
        private Color richtextboxbg;
        private Color richtextboxfg;
        private Color tabpagebg;
        private Color tabpagefg;

        public ColorScheme(Theme theme)
        {
            Color color2;
            if (theme == Theme.Dark)
            {
                this.PanelBG = color2 = SystemColors.ControlDark;
                this.TabPageBG = this.FormBG = color2;
                this.PanelFG = color2 = SystemColors.ControlText;
                this.TabPageFG = this.FormBG = color2;
            }
            else if (theme != Theme.DarkDark)
            {
                this.PanelBG = color2 = SystemColors.Control;
                this.TabPageBG = this.FormBG = color2;
                this.PanelFG = color2 = SystemColors.ControlText;
                this.TabPageFG = this.FormFG = color2;
            }
            else
            {
                this.PanelBG = color2 = SystemColors.ControlDarkDark;
                this.TabPageBG = this.FormBG = color2;
                this.PanelFG = color2 = SystemColors.ControlText;
                this.TabPageFG = this.FormFG = color2;
            }
        }

        public Color FormBG
        {
            get => 
                this.formbg;
            set => 
                this.formbg = value;
        }

        public Color FormFG
        {
            get => 
                this.formfg;
            set => 
                this.formfg = value;
        }

        public Color PanelBG
        {
            get => 
                this.panelbg;
            set => 
                this.panelbg = value;
        }

        public Color PanelFG
        {
            get => 
                this.panelfg;
            set => 
                this.panelfg = value;
        }

        public Color ButtonBG
        {
            get => 
                this.buttonbg;
            set => 
                this.buttonbg = value;
        }

        public Color ButtonFG
        {
            get => 
                this.buttonfg;
            set => 
                this.buttonfg = value;
        }

        public Color TextBoxBG
        {
            get => 
                this.textboxbg;
            set => 
                this.textboxbg = value;
        }

        public Color TextBoxFG
        {
            get => 
                this.textboxfg;
            set => 
                this.textboxfg = value;
        }

        public Color RichTextBG
        {
            get => 
                this.richtextboxbg;
            set => 
                this.richtextboxbg = value;
        }

        public Color RichTextFG
        {
            get => 
                this.richtextboxfg;
            set => 
                this.richtextboxfg = value;
        }

        public Color TabPageBG
        {
            get => 
                this.tabpagebg;
            set => 
                this.tabpagebg = value;
        }

        public Color TabPageFG
        {
            get => 
                this.tabpagefg;
            set => 
                this.tabpagefg = value;
        }

        public enum Theme
        {
            Light,
            Dark,
            DarkDark,
            Mahogany,
            TorrentLeech
        }
    }
}

