﻿using System;
using System.Runtime.InteropServices;

public static class KnownFolders
{
    private static string[] _knownFolderGuids;

    static KnownFolders()
    {
        string[] textArray1 = new string[11];
        textArray1[0] = "{56784854-C6CB-462B-8169-88E350ACB882}";
        textArray1[1] = "{B4BFCC3A-DB2C-424C-B029-7FE99A87C641}";
        textArray1[2] = "{FDD39AD0-238F-46AF-ADB4-6C85480369C7}";
        textArray1[3] = "{374DE290-123F-4565-9164-39C4925E467B}";
        textArray1[4] = "{1777F761-68AD-4D8A-87BD-30B759FA33DD}";
        textArray1[5] = "{BFB9D5E0-C6A9-404C-B2B2-AE6DB6AF4968}";
        textArray1[6] = "{4BD8D571-6D19-48D3-BE97-422220080E43}";
        textArray1[7] = "{33E28130-4E1E-4676-835A-98395C3BC3BB}";
        textArray1[8] = "{4C5C32FF-BB9D-43B0-B5B4-2D72E54EAAA4}";
        textArray1[9] = "{7D1D3A04-DEBB-4115-95CF-2F29DA2920DA}";
        textArray1[10] = "{18989B1D-99B5-455B-841C-AB7C74E4DDFC}";
        _knownFolderGuids = textArray1;
    }

    public static string GetPath(KnownFolder knownFolder) => 
        GetPath(knownFolder, false);

    public static string GetPath(KnownFolder knownFolder, bool defaultUser) => 
        GetPath(knownFolder, KnownFolderFlags.DontVerify, defaultUser);

    private static string GetPath(KnownFolder knownFolder, KnownFolderFlags flags, bool defaultUser)
    {
        IntPtr ptr;
        int errorCode = SHGetKnownFolderPath(new Guid(_knownFolderGuids[(int) knownFolder]), (uint) flags, new IntPtr(defaultUser ? -1 : 0), out ptr);
        if (errorCode < 0)
        {
            throw new ExternalException("Unable to retrieve the known folder path. It may not be available on this system.", errorCode);
        }
        Marshal.FreeCoTaskMem(ptr);
        return Marshal.PtrToStringUni(ptr);
    }

    [DllImport("Shell32.dll")]
    private static extern int SHGetKnownFolderPath([MarshalAs(UnmanagedType.LPStruct)] Guid rfid, uint dwFlags, IntPtr hToken, out IntPtr ppszPath);

    [Flags]
    private enum KnownFolderFlags : uint
    {
        SimpleIDList = 0x100,
        NotParentRelative = 0x200,
        DefaultPath = 0x400,
        Init = 0x800,
        NoAlias = 0x1000,
        DontUnexpand = 0x2000,
        DontVerify = 0x4000,
        Create = 0x8000,
        NoAppcontainerRedirection = 0x10000,
        AliasOnly = 0x80000000
    }
}

